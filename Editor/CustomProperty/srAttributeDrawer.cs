﻿/************************************************************************/
/*                            srAttributeDrawer                         */
/************************************************************************/

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System;
using System.Reflection;

[CustomPropertyDrawer( typeof( ReadOnlyAttribute ) )]
public class ReadOnlyAttributeDrawer : UnityEditor.PropertyDrawer
{
    public override void OnGUI ( Rect rect , SerializedProperty prop , GUIContent label )
    {
        bool wasEnabled = GUI.enabled;
        GUI.enabled = false;
        EditorGUI.PropertyField( rect , prop );
        GUI.enabled = wasEnabled;
    }
}

[CustomPropertyDrawer( typeof( SceneDropdownList ) )]
public class SceneDropdownListDrawer : PropertyDrawer
{
    string sceneFile;
    private const string SCENE_EXTENSION = ".unity";

    // 以後可改成 Array 避免每次Clear
    List<string> sceneNames = new List<string>();

    public override void OnGUI ( UnityEngine.Rect position , SerializedProperty property , UnityEngine.GUIContent label )
    {
        srAttribute attribute = ( srAttribute ) base.attribute;
        
        sceneNames.Clear();
        for ( int i = 0 ; i < EditorBuildSettings.scenes.Length ; i++ )
        {

            if ( EditorBuildSettings.scenes[ i ].enabled )
            {

                sceneFile = EditorBuildSettings.scenes[ i ].path.Substring( EditorBuildSettings.scenes[ i ].path.LastIndexOf( "/" ) + 1 );
                sceneNames.Add( sceneFile.Replace( SCENE_EXTENSION , string.Empty ) );
            }
        }

        // No Find Any Scene
        if ( sceneNames.Count == 0 )
        {
            EditorGUI.LabelField( position , label.text , "Class is empty." );
            property.stringValue = string.Empty;
            return;
        }

        // Avoid First Value Is Empty.
        if ( property.stringValue == string.Empty )
        {
            property.stringValue = sceneNames[ 0 ];
        }

        attribute.selected = sceneNames.FindIndex( x => x == property.stringValue );
        attribute.selected = EditorGUI.Popup( position , label.text , attribute.selected , sceneNames.ToArray() );
        if ( attribute.selected <0 )
            property.stringValue = sceneNames[ 0 ];
        else
            property.stringValue = sceneNames[ attribute.selected ];
    }
}

[CustomPropertyDrawer( typeof( ClassDropdownList ) )]
public class ClassDropdownListDrawer : PropertyDrawer
{
    Dictionary<string,int> TypeNames = new Dictionary<string, int>();
    List<string> TypeNameList = new List<string>();
    bool bInit;

    public override void OnGUI ( UnityEngine.Rect position , SerializedProperty property , UnityEngine.GUIContent label )
    {
        int index = 0;
        srAttribute attribute = ( srAttribute ) base.attribute;
        // Avoid repeat Call Foreach.
        if ( bInit == false )
        {
            System.Reflection.Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
            for ( int i = 0 ; i < assemblys.Length ; i++ )
            {
                System.Reflection.Assembly asm = assemblys[i];
                Type[] types = asm.GetTypes();
                for ( int j = 0 ; j < types.Length ; j++ )
                {
                    Type type = types[j];
                    if ( type.BaseType == attribute.m_Type )
                    {
                        string TypeName = type.ToString();
                        if ( !TypeNames.ContainsKey( TypeName ) )
                        {
                            //Debug.Log( TypeName );
                            TypeNames.Add( TypeName , index );
                            TypeNameList.Add( TypeName );
                            index++;
                        }
                    }
                }
            }
        }

        bInit = true;
        // No Find Any SubClass
        if ( TypeNameList.Count == 0 )
        {
            EditorGUI.LabelField( position , label.text , "Class is empty." );
            property.stringValue = string.Empty;
            return;
        }

        // Avoid Remove SubClass Error.
        if ( TypeNames.ContainsKey( property.stringValue ) == false )
        {
            property.stringValue = string.Empty;
        }

        // Avoid First Value Is Empty.
        if ( property.stringValue == string.Empty )
        {
            property.stringValue = TypeNameList[ 0 ];
        }

        attribute.selected = TypeNames[ property.stringValue ];
        attribute.selected = EditorGUI.Popup( position , label.text , attribute.selected , TypeNameList.ToArray() );

        property.stringValue = TypeNameList[ attribute.selected ];
    }
}

[CustomPropertyDrawer( typeof( EnumFlagAttribute ) )]
public class EnumFlagDrawer : PropertyDrawer
{
    public override void OnGUI ( Rect position , SerializedProperty property , GUIContent label )
    {
        EnumFlagAttribute flagSettings = ( EnumFlagAttribute ) attribute;
        Enum targetEnum = GetBaseProperty<Enum>( property );

        string propName = flagSettings.enumName;
        if ( string.IsNullOrEmpty( propName ) )
            propName = property.name;

        EditorGUI.BeginProperty( position , label , property );
        Enum enumNew = EditorGUI.EnumMaskField( position , propName , targetEnum );
        property.intValue = (int)Convert.ChangeType( enumNew , targetEnum.GetType() );
        EditorGUI.EndProperty();
    }

    static T GetBaseProperty<T> ( SerializedProperty prop )
    {
        // Separate the steps it takes to get to this property
        string[] separatedPaths = prop.propertyPath.Split( '.' );

        // Go down to the root of this serialized property
        System.Object reflectionTarget = prop.serializedObject.targetObject as object;
        // Walk down the path to get the target object
        foreach ( var path in separatedPaths )
        {
            FieldInfo fieldInfo = reflectionTarget.GetType().GetField( path );
            reflectionTarget = fieldInfo.GetValue( reflectionTarget );
        }
        return (T)reflectionTarget;
    }
}