﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System;
// makes sure that the static constructor is always called in the editor.
[InitializeOnLoad]
public class ComponentXCreator : Editor
{
    static GameObject[] selectedGameobjects;

    static ComponentXCreator ( )
    {
        //Debug.Log( "ComponentXCreator" );

        // Adds a callback for when the hierarchy window processes GUI events
        // for every GameObject in the heirarchy.
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemCallback;
        EditorApplication.hierarchyWindowChanged += HierarchyWindowChanged;
    }
    ~ComponentXCreator ( )
    {
        //Debug.Log( "destructor " );
        EditorApplication.hierarchyWindowItemOnGUI -= HierarchyWindowItemCallback;
        EditorApplication.hierarchyWindowChanged -= HierarchyWindowChanged;
    }

    private static void HierarchyWindowChanged ( )
    {
        if ( selectedGameobjects!= null && selectedGameobjects.Length > 0 )
            Selection.objects = selectedGameobjects;
    }

    static void HierarchyWindowItemCallback ( int pID , Rect pRect )
    {
        var eventType = Event.current.type;
        // happens when an acceptable item is released over the GUI window
        if ( eventType == EventType.DragExited )
        {
            //Debug.Log( "exit" );

            // get all the drag and drop information ready for processing.
            DragAndDrop.AcceptDrag();
            // used to emulate selection of new objects.
            var selectedObjects = new List<GameObject>();
            // run through each object that was dragged in.
            for ( int i = 0 ; i < DragAndDrop.objectReferences.Length ; i++ )
            {
                UnityEngine.Object objectRef = DragAndDrop.objectReferences[i];
                if ( objectRef == null )
                    return;
                bool isEditorScript = objectRef.GetType().Equals(typeof(MonoScript));
                if ( isEditorScript == false )
                    break;

                string scriptName = objectRef.name;
                var type = srEditorUtility.GetType(scriptName);
                //Debug.Log( type );

                // if the object is the particular asset type...
                if ( type.BaseType == typeof( MonoBehaviour ) )
                {
                    Component mono = objectRef as Component;
                    // we create a new GameObject using the asset's name.
                    var gameObject = new GameObject(objectRef.name);
                    // we attach component X, associated with asset X.
                    var componentX = gameObject.AddComponent(type);
                    // we place asset X within component X.
                    //componentX.assetX = objectRef as AssetX;
                    // add to the list of selected objects.
                    selectedObjects.Add( gameObject );
                }
            }

            // we didn't drag any assets of type AssetX, so do nothing.
            if ( selectedObjects.Count == 0 ) return;
            // emulate selection of newly created objects.
            selectedGameobjects = selectedObjects.ToArray();
            // make sure this call is the only one that processes the event.
            Event.current.Use();
        }
    }

}