﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
/// http://answers.unity3d.com/questions/516588/attach-component-by-dragging-an-asset-to-a-gameobj.html
// Create UGUI Image by Dragging Texture2D to Canvas of Hierarchy.
// makes sure that the static constructor is always called in the editor.
[InitializeOnLoad]
public class ImageCreator : Editor
{
    static bool CanAddImage;
    static ImageCreator ( )
    {
        CanAddImage = true;
        // Adds a callback for when the hierarchy window processes GUI events
        // for every GameObject in the heirarchy.
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemCallback;
    }

    static void HierarchyWindowItemCallback ( int pID , Rect pRect )
    {
        //Debug.Log( Event.current.type );
        if ( Event.current.type == EventType.Layout )
        {
            CanAddImage = true;
        }
        // happens when an acceptable item is released over the GUI window
        else if ( Event.current.type == EventType.DragExited && CanAddImage )
        {
            if ( DragAndDrop.objectReferences.Length == 0 )
                return;
            object obj = DragAndDrop.objectReferences[ 0 ];
            GameObject go = obj as GameObject;
            SpriteRenderer spriteRenderer = null;
            if ( go != null )
                spriteRenderer = go.GetComponent<SpriteRenderer>();
            bool IsSpriteOrTexture2D = obj is Texture2D || spriteRenderer != null;
            if ( IsSpriteOrTexture2D == false )
                return;

            // get all the drag and drop information ready for processing.
            DragAndDrop.AcceptDrag();

            if ( Selection.activeGameObject.transform.GetComponentInParent<Canvas>() != null )
            {
                CanAddImage = false;
                spriteRenderer = Selection.activeGameObject.GetComponent<SpriteRenderer>();
                Sprite sprite = spriteRenderer.sprite;
                Image image = Selection.activeGameObject.AddComponent<Image>();
                image.sprite = sprite;
                var imageRectTransform = image.transform as RectTransform;
                imageRectTransform.sizeDelta = new Vector2( 100 , 100 );
                DestroyImmediate( spriteRenderer );
                Selection.activeGameObject.transform.localPosition = Vector3.zero;
            }
        }
    }
}