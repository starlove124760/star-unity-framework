﻿using System;
using UnityEditor;

//[InitializeOnLoad]
public class srEditorUtility
{
    // setup once on load
    static srEditorUtility ( )
    {
        EditorApplication.update += OnUpdate;
    }

    // called 100 times / sec
    private static void OnUpdate ( )
    {
        if ( EditorApplication.isCompiling )
            EditorApplication.isPlaying = false;
    }

    public static Type GetType ( string typeName )
    {
        Type result = null;
        System.Reflection.Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
        for ( int i = 0 ; i < assemblys.Length ; i++ )
        {
            System.Reflection.Assembly asm = assemblys[i];
            Type[] types = asm.GetTypes();
            for ( int j = 0 ; j < types.Length ; j++ )
            {
                Type type = types[j];
                if ( type.Name.Equals( typeName ) )
                {
                    result = type;
                    break;
                }
            }
        }
        return result;
    }
}