﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

public class GameObjectContextMenuEditor : EditorWindow
{
    [MenuItem( "GameObject/srToolBox/DeleteUnderThisAllCollider" , false , 0 )]
    static void DelectUnderCollider ( )
    {
        GameObject SelectedGo = Selection.activeObject as GameObject;
        Collider[ ] boxColiiders = SelectedGo.GetComponentsInChildren<Collider>();
        //Undo.RecordObjects( boxColiiders ,"UndoBoxCollider");
        GameObject[ ] goArray = boxColiiders.Select( box => box.gameObject ).ToArray();
        foreach ( var collider in boxColiiders )
        {
            Debug.Log( "Delete [" + collider.GetType().Name + " ] From " + collider );
            Undo.DestroyObjectImmediate( collider );
            Object.DestroyImmediate( collider );
        }
        Selection.objects = goArray;
    }

    [MenuItem( "GameObject/srToolBox/DeleteUnderThisAllEmptyAnimator" , false , 0 )]
    static void DelectUnderEmptyAnimator ( )
    {
        GameObject SelectedGo = Selection.activeObject as GameObject;
        List<GameObject> PingList = new List<GameObject>();

        foreach ( var animator in SelectedGo.GetComponentsInChildren<Animator>() )
        {
            if ( animator.avatar == null && animator.runtimeAnimatorController == null )
            {
                Debug.Log( "Delete Animator From " + animator );
                PingList.Add( animator.gameObject );
                Undo.DestroyObjectImmediate( animator );

                Object.DestroyImmediate( animator );
            }
        }
        Selection.objects = PingList.ToArray();
    }
}