﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class DefineSymbolWindow : EditorWindow
{
    string Symbol = string.Empty;

    void OnEnable ( )
    {

    }

    void OnDisable ( )
    {
    }

    void OnGUI ( )
    {
        BeginWindows();

        string Symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup( BuildTargetGroup.Standalone );
        GUILayout.Label( "Current Symbol String : " + Symbols );

        Symbol = GUILayout.TextField( Symbol , 17 );

        GUILayout.Space( 20 );

        bool addStringToRelease = GUILayout.Button( "Add Release" );
        if ( addStringToRelease )
        {
            Symbol = "Release";
        }

        GUILayout.Space( 20 );
        if ( string.IsNullOrEmpty( Symbol ) == false )
        {
            bool addSymbolBtnClicked = GUILayout.Button( "ADD Symbol" );
            if ( addSymbolBtnClicked )
            {
                HandleSymbol( 1 , Symbols , Symbol );
                //this.Close();
            }

            bool removeSymbolBtnClicked = GUILayout.Button( "Remove Symbol" );
            if ( removeSymbolBtnClicked )
            {
                HandleSymbol( 2 , Symbols , Symbol );
                //this.Close();
            }
        }

        EndWindows();
    }

    /// <summary>
    /// 管理加入與移除Symbol
    /// </summary>
    /// <param name="type">1:ADD , 2:Remove</param>
    /// <param name="symbolsString"></param>
    /// <param name="define"></param>
    void HandleSymbol ( int type , string symbolsString , string define )
    {
        string scriptingDefineSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup( BuildTargetGroup.Standalone );
        List<string> definesList = new List<string>( scriptingDefineSymbols.Split( ';' ) );
        //Add
        if ( definesList.Contains( define ) == false && type == 1 )
        {
            definesList.Add( define );
            Debug.Log( "Scripting Define Symbol Added To [Project Settings->Player]: " + define );
        }
        //Remove
        else if ( definesList.Contains( define ) && type == 2 )
        {
            definesList.Remove( define );
            Debug.Log( "Scripting Define Symbol Removed from [Project Settings->Player]: " + define );
        }
        PlayerSettings.SetScriptingDefineSymbolsForGroup( BuildTargetGroup.Standalone , string.Join( ";" , definesList.ToArray() ) );
    }
}