﻿using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class StarToolsManager
{
    static string AssetProjectPath;
    static StarToolsManager ( )
    {
        AssetProjectPath = GetAssetProjectPath();
    }
    static string GetAssetProjectPath ( )
    {
        if ( string.IsNullOrEmpty( AssetProjectPath ) )
        {
            string dataPath = Application.dataPath;
            string[ ] everyDataPath = dataPath.Split( '/' );
            int everyDataPathLength = everyDataPath.Length;
            string ProjectName = everyDataPath[ everyDataPathLength - 2 ];
            AssetProjectPath = "Assets/" + ProjectName;
        }
        return AssetProjectPath;
    }


    [MenuItem( "StarTools/Open PlayerSettings Window _F1" )]
    static void OpenPlayerSettings ( )
    {
        EditorApplication.ExecuteMenuItem( "Edit/Project Settings/Player" );
    }

    [MenuItem( "StarTools/Toggle VR Supported _F3" )]
    static void VRSettingEnable ( )
    {
        bool virtualRealitySupported = PlayerSettings.virtualRealitySupported;
        PlayerSettings.virtualRealitySupported = !virtualRealitySupported;
        Debug.Log( "Now VirtualRealitySupported Is " + !virtualRealitySupported );
    }

    [MenuItem( "StarTools/Toggle Inspector Lock _F4" )]
    static void ToggleInspectorLock ( )
    {
        ActiveEditorTracker.sharedTracker.isLocked = !ActiveEditorTracker.sharedTracker.isLocked;
        ActiveEditorTracker.sharedTracker.ForceRebuild();
    }

    [MenuItem( "StarTools/AddScriptingDefineSymbols _F5" )]
    static void AddScriptingDefineSymbols ( )
    {
        EditorWindow.GetWindow( typeof( DefineSymbolWindow ) );
    }

    //[MenuItem( "StarTools/Copy Asset Path to ClipBoard" )]
    //static void CopyAssetPath2Clipboard ( )
    //{
    //    //string path = AssetDatabase.GetAssetPath(Selection.activeInstanceID);
    //    string path = Application.dataPath;
    //    string path2 = AssetDatabase.GetAssetPath(Selection.activeInstanceID);
    //    path2 = path2.Remove( 0 , 6 );
    //    path = path + path2;
    //    path = path.Replace( '/' , '\\' );
    //    //path = path.Replace('!', '');
    //    //string path = Debug.Log(AssetDatabase.GetAssetPath(material));
    //    TextEditor text2Editor = new TextEditor();
    //    text2Editor.text = path;
    //    text2Editor.OnFocus();
    //    text2Editor.Copy();
    //}
}