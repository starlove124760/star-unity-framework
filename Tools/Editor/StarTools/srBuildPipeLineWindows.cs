﻿using UnityEngine;
using UnityEditor;
using Process = System.Diagnostics.Process;

public class srBuildPipeLineWindows : EditorWindow
{
    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    bool buttonDown;
    float myFloat = 1.23f;
    BuildScriptableObject buildScriptable;

    //[MenuItem( "srTools/Windows Build With Postprocess _F5" )]
    static void Init ( )
    {
        // Get existing open window or if none, make a new one:
        srBuildPipeLineWindows window = (srBuildPipeLineWindows)GetWindow( typeof( srBuildPipeLineWindows ) );
        window.Show();
    }

    void OnGUI ( )
    {
        GUILayout.Label( "Base Settings" , EditorStyles.boldLabel );
        myString = EditorGUILayout.TextField( "Text Field" , myString );
        buttonDown = GUILayout.Button( "Build Exe" );
        buildScriptable = (BuildScriptableObject)EditorGUILayout.ObjectField( buildScriptable , typeof( UnityEngine.Object ) , true );


        groupEnabled = EditorGUILayout.BeginToggleGroup( "Optional Settings" , groupEnabled );
        myBool = EditorGUILayout.Toggle( "Toggle" , myBool );
        myFloat = EditorGUILayout.Slider( "Slider" , myFloat , -3 , 3 );
        EditorGUILayout.EndToggleGroup();

        if ( buttonDown )
        {
            Build();
        }
    }

    void Build ( )
    {
        OverrideScriptableObject();
        AutoBuild();
    }

    void OverrideScriptableObject ( )
    {
        buildScriptable.num += 10;
    }


    void AutoBuild ( )
    {
        // Get filename.
        string path = EditorUtility.SaveFolderPanel( "Choose Location of Built Game" , "Exe" , "" );
        string[] levels = new string[] { "Assets/Scene1.unity" , "Assets/Scene2.unity" };

        // Build player.
        BuildPipeline.BuildPlayer( levels , path + "/BuiltGame.exe" , BuildTarget.StandaloneWindows , BuildOptions.None );

        // Copy a file from the project folder to the build folder, alongside the built game.
        //FileUtil.CopyFileOrDirectory( "Assets/Templates/Readme.txt" , path + "Readme.txt" );

        // Run the game (Process class from System.Diagnostics).
        Process proc = new Process();
        proc.StartInfo.FileName = path + "/BuiltGame.exe";
        proc.Start();
    }
}