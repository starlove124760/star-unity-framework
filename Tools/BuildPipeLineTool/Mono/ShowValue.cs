﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowValue : MonoBehaviour {
    public BuildScriptableObject  buildScriptableObject;
    // Use this for initialization
    void Start () {
        print( buildScriptableObject.num );
	}
	
	// Update is called once per frame
	void Update () {
        if ( Input.GetKeyDown( KeyCode.Space ) )
        {
            buildScriptableObject.num++;
            Debug.Log( buildScriptableObject.num );
        }
	}
}
