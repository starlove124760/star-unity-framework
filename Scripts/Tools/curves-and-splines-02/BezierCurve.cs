﻿using UnityEngine;

public class BezierCurve : MonoBehaviour
{

    public Vector3[ ] points;

    public Vector3 GetPoint ( float t )
    {
        return transform.TransformPoint( Bezier.GetPoint( points[ 0 ] , points[ 1 ] , points[ 2 ] , points[ 3 ] , t ) );
    }

    public Vector3 GetVelocity ( float t )
    {
        return transform.TransformPoint( Bezier.GetFirstDerivative( points[ 0 ] , points[ 1 ] , points[ 2 ] , points[ 3 ] , t ) ) - transform.position;
    }

    public Vector3 GetDirection ( float t )
    {
        return GetVelocity( t ).normalized;
    }

    public void Reset ( )
    {
        points = new Vector3[ ] {
            new Vector3(1f, 0f, 0f),
            new Vector3(2f, 0f, 0f),
            new Vector3(3f, 0f, 0f),
            new Vector3(4f, 0f, 0f)
        };
    }

    //public void SetDinoMoveCurvePoint ( Transform start , Transform end , PathDirectionType pathDirectionType ,
    //    AttackStandPlaceType attackStandPlaceType )
    //{
    //    int randValue = Random.Range( 0 , 2 );
    //    bool isRight = randValue == 1;
    //    Vector3 dir = Vector3.right;
    //    switch ( pathDirectionType )
    //    {
    //        case PathDirectionType.OnlyLeft:
    //            dir = Vector3.left;
    //            break;
    //        case PathDirectionType.OnlyRight:
    //            break;
    //        case PathDirectionType.both:
    //            dir = isRight ? Vector3.right : Vector3.left;
    //            break;
    //        default:
    //            break;
    //    }
    //    dir *= 2;
    //    start.LookAt( end );
    //    Vector3 pos = start.position;
    //    Vector3 targetPos = end.position;
    //    float distance = Vector3.Distance( pos , targetPos );
    //    float multiply = distance / 12;
    //    Vector3 offsetPoint2 = dir * multiply + Vector3.forward * multiply * 2;
    //    if ( attackStandPlaceType == AttackStandPlaceType.back )
    //    {
    //        offsetPoint2 = dir * multiply * 2 + Vector3.forward * multiply * 4;
    //    }
    //    targetPos.y = 0;
    //    points[ 0 ] = pos;
    //    points[ 1 ] = start.TransformPoint( dir * multiply + Vector3.forward * multiply );
    //    points[ 2 ] = start.TransformPoint( offsetPoint2 );
    //    points[ 3 ] = targetPos;
    //}

    public void SetDinoJumpCurvePoint ( Transform start , Transform end )
    {
        int randValue = Random.Range( 0 , 2 );
        bool isRight = randValue == 1;
        //start.LookAt( end );
        Vector3 pos = start.position;
        Vector3 targetPos = end.position;
        int OffsetZ = ( pos.z - targetPos.z ) > 0 ? -1 : 1;
        float distance = Vector3.Distance( pos , targetPos );
        float multiply = Mathf.Abs( pos.x - targetPos.x ) / 2;
        float z = Mathf.Abs( pos.z - targetPos.z ) / 2;
        Vector3 CenterVector = pos + Vector3.right * multiply + Vector3.up * 5f + Vector3.forward * z * OffsetZ;
        //-25.6 , -1 ,35.3f
        //-2.18 ,-1.12, 37

        targetPos.y = pos.y;
        points[ 0 ] = pos;
        points[ 1 ] = CenterVector;
        points[ 2 ] = CenterVector;
        points[ 3 ] = targetPos;
    }
}