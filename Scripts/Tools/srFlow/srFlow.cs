﻿
public enum srFlowEnum
{
    Normal,
    Message,
    Delay,
}

public class srFlow
{
    public srCallBackV1 m_srCallbackV1;
    public srCallBack   m_srCallBack;
    public string       m_ListenMessage;
    public float        m_DelayTime;
    public srFlowEnum   m_flowEnum;

    public bool IsComplete;
    public int  playIndex;


    public srFlow ( srCallBack tsrCallBack  )
    {
        m_srCallBack = tsrCallBack;
        m_flowEnum = srFlowEnum.Normal;
    }

    public srFlow ( srCallBackV1 tsrCallBackV1 )
    {
        m_srCallbackV1 = tsrCallBackV1;
        m_flowEnum = srFlowEnum.Normal;
    }
}
