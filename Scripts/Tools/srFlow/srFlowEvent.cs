﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// srFlowEvent註冊後回傳的Class
/// </summary>
public class srFlowClass
{
    public  List<srFlow> flowList ;
    public  int id;
    private int index;
    bool bInit;

    public srFlowClass ( int _id )
    {
        flowList = new List<srFlow>();
        id = _id;
        bInit = true;
    }

    public srFlow f_RegEvent ( srCallBack tsrCallBack )
    {
        srFlow _srFlow = new srFlow( tsrCallBack );
        flowList.Add( _srFlow );
        return _srFlow;
    }

    public srFlow f_RegEvent ( srCallBackV1 tsrCallBackV1 )
    {
        srFlow _srFlow = new srFlow( tsrCallBackV1 );
        flowList.Add( _srFlow );
        return _srFlow;
    }

    void f_ResetFlow ( )
    {
        for ( int i = 0 ; i < flowList.Count ; i++ )
        {
            srFlow _srFlow = flowList[ i ];
            _srFlow.ResetFlow();
        }
        bInit = true;
    }

    public void f_DoEvent ( )
    {
        if ( bInit == false )
        {
            index = 0;
            f_ResetFlow();
        }
        DoEvent();
    }

    bool show;
    private void DoEvent ( )
    {
        if ( show == false )
        {
            show = true;
            Debug.Log( "Count : " + flowList.Count );
        }

        if ( flowList[ index ].IsComplete == true )
            index++;

        if ( index >= flowList.Count )
        {
            Debug.Log( "全部流程執行完畢！" );
            bInit = false;
            return;
        }
        Debug.Log( "index : " + index );

        srFlow _srFlow = flowList[ index ];

        float _DelayTime = _srFlow.m_DelayTime;
        _srFlow.IsComplete = true;

        if ( _srFlow.m_flowEnum == srFlowEnum.Delay )
        {
            Debug.Log( "延遲 ： " + _DelayTime + " 秒，呼叫下一個事件" );

            srTimerEvent.Instance.f_RegEvent( _DelayTime , _srFlow.m_srCallBack );
            srTimerEvent.Instance.f_RegEvent( _DelayTime , f_DoEvent );
        }
        else if ( _srFlow.m_flowEnum == srFlowEnum.Message )
        {
            Debug.Log( "等待按下鍵盤 ： " + _srFlow.m_ListenMessage );
            srMessage.AddListener( _srFlow.m_ListenMessage , _srFlow.m_srCallbackV1 );
            srMessage.AddListener( _srFlow.m_ListenMessage , RemoveListener );
        }
        else
        {
            _srFlow.m_srCallBack();
            f_DoEvent();
        }
    }

    private void RemoveListener ( object data )
    {
        srFlow _srFlow = flowList[ index ];
        //print( "remove" );
        srMessage.RemoveListener( "C" );
        srMessage.RemoveListener( "F" );
        f_DoEvent();
    }
}

public class srFlowEvent
{
    //private static srFlowEvent instance;
    //public static srFlowEvent Instance
    //{
    //    get {
    //        if ( instance == null )
    //        {
    //            if ( ccCommonClass.ccEngineSingletonObj == null )
    //            {
    //                srFlowManager = new GameObject( "srFlowManager" );
    //                DontDestroyOnLoad( srFlowManager );
    //            }
    //            instance = srFlowManager.AddComponent<srFlowEvent>();

    //            if ( instance == null )
    //            {
    //                Debug.LogError( "Init srFlowEvent Fail" );
    //            }
    //        }
    //        return instance;
    //    }
    //}
    //static GameObject srFlowManager;
    static int flowIndex;

    static List<srFlowClass> flowClasslist = new List<srFlowClass>();

    public static srFlowClass f_RegFlow ( )
    {
        srFlowClass _srFlowClass = new srFlowClass( flowIndex );
        flowIndex++;
        flowClasslist.Add( _srFlowClass );
        return _srFlowClass;
    }
}
