﻿using System.Collections.Generic;
using UnityEngine;

//public delegate void ccCallback ();
public class srMessage
{
    private static Dictionary<string , ObjList> eventTable = new Dictionary<string , ObjList>();

    public static void AddListener ( string eventType , srCallBackV1 handler )
    {
        ObjList list;
        if ( eventTable.TryGetValue( eventType , out list ) )
        {
            list.f_Add( handler );
        }
        else
        {
            list = new ObjList();
            list.f_Add( handler );
            eventTable.Add( eventType , list );
        }
    }

    public static void AddListener ( System.Enum eventType , srCallBackV1 handler )
    {
        AddListener( eventType.ToString() , handler );
    }

    public static void Broadcast ( string eventType , object data = null )
    {
        ObjList list;
        if ( eventTable.TryGetValue( eventType , out list ) )
        {
            list.f_Broadcast( data );
        }
    }

    public static void Broadcast ( System.Enum eventType , object data = null )
    {
        Broadcast( eventType.ToString() , data );
    }

    public static void Clear ( )
    {
        eventTable.Clear();
    }
   
    public static void RemoveListener ( string eventType )
    {
        ObjList list;
        if ( eventTable.TryGetValue( eventType , out list ) )
        {
            eventTable.Remove( eventType );
        }
    }

    public static void RemoveListener ( string eventType , srCallBackV1 handler )
    {
        ObjList list;
        if ( eventTable.TryGetValue( eventType , out list ) )
        {
            list.f_Remove( handler );
        }
    }

    public static void RemoveListener ( System.Enum eventType )
    {
        RemoveListener( eventType.ToString() );
    }

    public static void RemoveListener ( System.Enum eventType , srCallBackV1 handler )
    {
        RemoveListener( eventType.ToString() , handler );
    }

}
