﻿using UnityEngine;

public class test_srCallBackV1 : MonoBehaviour
{
        enum EventType
        {
                Test123,
                Test456
        }
        // Use this for initialization
        void Start ()
        {
                srMessage.AddListener( EventType.Test123.ToString() , callback );
                srMessage.AddListener( EventType.Test123.ToString() , callback );
                srMessage.AddListener( EventType.Test123.ToString() , callback2 );
                srMessage.Broadcast( EventType.Test123.ToString() , EventType.Test456 );
                //整體Remove該事件
                //srCallBackV1.f_RemoveListener( EventType.Test123.ToString() );
                //單獨Remove 該Callback
                srMessage.RemoveListener( EventType.Test123.ToString() , callback2 );
                srMessage.Broadcast( EventType.Test123.ToString() , EventType.Test456 );
        }

        private void callback2 ( object data )
        {
                print( "callback2 " + ( EventType ) data );
        }
        private void callback ( object data )
        {
                print( "callback " + ( EventType ) data );
        }

}
