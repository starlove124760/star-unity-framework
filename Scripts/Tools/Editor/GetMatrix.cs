﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class GetMatrix : MonoBehaviour {
    public Vector3 euler;

	void Update () {
        var transformEuler = new SerializedObject(transform).FindProperty("m_LocalEulerAnglesHint");
        euler.Set(transformEuler.vector3Value[0]
            , transformEuler.vector3Value[1]
            , transformEuler.vector3Value[2]);
    }
}
