﻿using UnityEngine;
using System.Collections.Generic;

public class GameObjectFinder
{
    public static List<GameObject> objList = new List<GameObject>();
    private static readonly string GreenColorCode = "00DB00";

    /// <summary>
    /// 透過在場景中的名字去取得遊戲物件，必須先在場景拉入[GameObjectRegsiter]並將物件放入。
    /// </summary>
    /// <param name="goName"></param>
    /// <returns></returns>
    public static GameObject GetObj ( string goName )
    {
        GameObject go = objList.Find( _go => _go != null && _go.name == goName );

        if ( go )
        {
            return go;
        }
        else
        {
            Debug.LogError( "[ GameObjectFinder Class] ObjList Don't Have This Name : " + srString.GetBoldColorString( GreenColorCode , goName ) );
            return null;
        }
    }

    /// <summary>
    /// 透過在場景中的名字去取得遊戲物件Transform，必須先在場景拉入[GameObjectRegsiter]並將物件放入。
    /// </summary>
    /// <param name="goName"></param>
    /// <returns></returns>
    public static Transform GetObjTransform ( string goName )
    {
        GameObject go = GetObj( goName );
        return go.transform;
    }

    /// <summary>
    /// 透過在場景中的名字去取得遊戲物件，必須先在場景拉入[GameObjectRegsiter]並將物件放入。
    /// </summary>
    /// <param name="goName"></param>
    /// <returns></returns>
    public static List<GameObject> GetObjs ( string goName )
    {
        List<GameObject> go = objList.FindAll( _go => _go.name == goName );

        if ( go.Count > 0 )
        {
            return go;
        }
        else
        {
            Debug.LogError( "[ GameObjectFinder Class] ObjList Don't Have This Name : " + srString.GetBoldColorString( GreenColorCode , goName ) );
            return null;
        }
    }
}
