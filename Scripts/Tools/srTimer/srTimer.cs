﻿
public class srTimer
{
    public float m_DelayTime;
    public float m_CurTime;

    public int m_RepeatCount = 1;
    public srCallBack m_srCallBack;

    public srTimer ( float DelayTime , srCallBack tsrCallBack )
    {
        m_srCallBack = tsrCallBack;
        m_DelayTime = DelayTime;
        m_CurTime = DelayTime;
    }

    public void Reset ( )
    {
        m_CurTime = m_DelayTime;
    }
}
