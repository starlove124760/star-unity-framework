﻿using System;
using DG.Tweening;
using UnityEngine;
//using DG.Tweening;
public class TestSrFlowEvent : MonoBehaviour
{
    string message;
    srFlowClass flowClass;

    // Use this for initialization
    void Start ( )
    {
        //transform.DOMoveX( 45 , 1 ).SetDelay( 2 ).SetEase( Ease.OutQuad ).OnComplete( ( ) => {
        //    print( "Complete" );
        //} );

        flowClass = srFlowEvent.f_RegFlow();
        flowClass.f_RegEvent( ( ) => { print( "A" ); } );
        flowClass.f_RegEvent( ( ) => { print( "A1" ); } );
        flowClass.f_RegEvent( ( ) => { print( "B" ); } ).SetDelayTime( 2 );
        flowClass.f_RegEvent( ( object o ) => { print( "C" ); } ).AddListener( "C" );
        flowClass.f_RegEvent( ( ) => { print( "D" ); } );
        flowClass.f_RegEvent( ( ) => { print( "E" ); } ).SetDelayTime( 2 );
        flowClass.f_RegEvent( ( object o ) => { print( "F" ); } ).AddListener( "F" );
        flowClass.f_DoEvent();
    }

    // Update is called once per frame
    void Update ( )
    {
        if ( Input.GetKeyDown( KeyCode.Space ) )
        {
            print( "Pressed Space 重新整個流程" );
            flowClass.f_DoEvent();
        }
        if ( Input.GetKeyDown( KeyCode.C ) )
        {
            print( "Pressed C" );
            srMessage.Broadcast( "C" );
        }
        if ( Input.GetKeyDown( KeyCode.F ) )
        {
            print( "Pressed F" );
            srMessage.Broadcast( "F" );
        }
    }
}
