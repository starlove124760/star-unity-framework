﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class srTimerEvent : MonoBehaviour
{
    private static srTimerEvent instance;
    public static srTimerEvent Instance
    {
        get {
            if ( instance == null )
            {
                if ( ccCommonClass.ccEngineSingletonObj == null )
                {
                    srTimerManager = new GameObject( "srTimerManager" );
                    DontDestroyOnLoad( srTimerManager );
                }
                instance = srTimerManager.AddComponent<srTimerEvent>();

                if ( instance == null )
                {
                    Debug.LogError( "Init srTimerEvent Fail" );
                }
            }
            return instance;
        }
    }

    static GameObject srTimerManager;
    bool IsInit;
    float invokeTime = 0.1f;
    List<srTimer> timerList = new List<srTimer>();
    // Use this for initialization
    void Start ( )
    {
        //Remove other scripts.
        if ( instance != this )
            Destroy( this );
    }

    void Init ( )
    {
        //print( "Init" ); 
        CancelInvoke( "CustomCall" );
        InvokeRepeating( "CustomCall" , invokeTime , invokeTime );
        IsInit = true;
    }


    void CustomCall ( )
    {
        //print( timerList.Count );
        if ( timerList.Count > 0 )
        {
            lock ( timerList )
            {
                for ( int i = 0 ; i < timerList.Count ; i++ )
                {
                    srTimer _Timer = timerList[ i ];
                    if ( _Timer.m_srCallBack == null )
                        return;
                    _Timer.m_CurTime -= invokeTime;
                    if ( _Timer.m_CurTime <= 0 )
                    {
                        _Timer.m_srCallBack();
                        _Timer.m_RepeatCount--;
                        if ( _Timer.m_RepeatCount <= 0 )
                        {
                            timerList.Remove( _Timer );
                            IsInit = false;
                        }
                        else
                            _Timer.Reset();
                    }
                }
            }
        }
        else
        {
            CancelInvoke( "CustomCall" );
        }
    }

    public srTimer f_RegEvent ( float DelayTime , srCallBack tsrCallBack )
    {
        if ( IsInit == false )
        {
            Init();
        }
        //print( "Reg" );
        srTimer _timer = new srTimer( DelayTime , tsrCallBack );
        timerList.Add( _timer );
        return _timer;
    }
    
}
