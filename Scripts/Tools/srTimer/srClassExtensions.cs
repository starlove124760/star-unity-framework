﻿
public static class srClassExtensions
{

    #region srTimer
    public static void SetRepeatCount ( this srTimer tsrTimer , int tCount )
    {
        tsrTimer.m_RepeatCount = tCount;
    }
    #endregion

    #region srFlow
    public static void SetDelayTime ( this srFlow tsrFlow , float tDelayTime )
    {
        tsrFlow.m_DelayTime = tDelayTime;
        tsrFlow.m_flowEnum = srFlowEnum.Delay;
    }

    public static void AddListener ( this srFlow tsrFlow , System.Enum eventType )
    {
        AddListener( tsrFlow , eventType.ToString() );
    }

    public static void AddListener ( this srFlow tsrFlow , string eventType )
    {
        tsrFlow.m_ListenMessage = eventType;
        tsrFlow.m_flowEnum = srFlowEnum.Message;
    }

    public static void ResetFlow ( this srFlow tsrFlow )
    {
        tsrFlow.IsComplete = false;
    }

    #endregion
}
