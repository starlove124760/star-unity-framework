﻿using System;
using UnityEngine;
public class srAttribute : PropertyAttribute
{
    public int selected;
    public Type m_Type;
}

/// <summary>
/// 只能使用在String上，否則會出錯
/// </summary>
[AttributeUsage( AttributeTargets.Field )]
public class ClassDropdownList : srAttribute
{
    public ClassDropdownList ( Type type )
    {
        m_Type = type;
    }
}

/// <summary>
/// 只能使用在String上，否則會出錯
/// </summary>
[AttributeUsage( AttributeTargets.Field )]
public class SceneDropdownList : srAttribute
{
}

/// <summary>
/// Enum使用，會增加ALL跟Nothing
/// </summary>
public class EnumFlagAttribute : PropertyAttribute
{
    public string enumName;

    public EnumFlagAttribute ( )
    {
    }

    public EnumFlagAttribute ( string name )
    {
        enumName = name;
    }
}

// Restrict to methods only
[AttributeUsage( AttributeTargets.Method )]
public class ExposeMethodInEditorAttribute : Attribute
{
}

/// <summary>Replacement for RPC attribute with different name. Used to flag methods as remote-callable.</summary>
public class PhotonRPC : Attribute
{
}

[AttributeUsage( AttributeTargets.Field , Inherited = true )]
public class ReadOnlyAttribute : PropertyAttribute { }

namespace Star_unity_framework
{
    /// <summary>
    /// 給Enum使用，賦予一個Type，為了將來動態實例化使用
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    class ClassInstanceAttribute : Attribute
    {
        public Type type
        {
            get; private set;
        }

        public ClassInstanceAttribute ( Type ty )
        {
            type = ty;
        }
    }
}

