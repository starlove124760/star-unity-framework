﻿using UnityEngine;
using System.Collections;
using Star_unity_framework.Design_Patterns.Strategy;

public class srAttributeExample : MonoBehaviour
{
    [ClassDropdownList(typeof(BaseLogClass))]
    public string class_11;

    [SceneDropdownList]
    public string Scene_11;

    public TriggerTarget sa;
    // Use this for initialization
    void Start ( )
    {
        ColliderStrategy strategy = (ColliderStrategy)System.Activator.CreateInstance(
           sa.GetTypeOfThisEnum() );
    }

    // Update is called once per frame
    void Update ( )
    {
        if ( Input.GetKeyDown( KeyCode.Space ) )
        {
            Debug.Log( class_11 );
        }
        if ( Input.GetKeyDown( KeyCode.C ) )
        {
            Debug.Log( Scene_11 );
        }
        if ( Input.GetKeyDown( KeyCode.F ) )
        {
        }
    }
}
