﻿
using System.Collections.Generic;

internal class ObjList
{
    private List<srCallBackV1> m_List = new List<srCallBackV1>();

    public void f_Add ( srCallBackV1 handler )
    {
        m_List.Add( handler );
    }

    public void f_Broadcast ( object data )
    {
        int count = m_List.Count;

        for ( int i = 0 ; i < count ; i++ )
        {
            srCallBackV1 callBackFun = m_List[ i ];
            if ( callBackFun != null )
            {
                callBackFun( data );
            }
        }

        //反向 怕中間被刪除
        //for ( int i = count - 1 ; i >= 0 ; i-- )
        //{
        //    ccCallbackV1 callBackFun = m_List[ i ];
        //    if ( callBackFun != null )
        //    {
        //        callBackFun( data );
        //    }
        //}
    }

    public void f_Remove ( srCallBackV1 handler )
    {
        srCallBackV1 data = m_List.Find( callback => callback == handler );
        if ( data != null )
        {
            m_List.Remove( data );
        }
    }
}
