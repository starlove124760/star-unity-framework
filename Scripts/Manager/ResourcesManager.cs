﻿using UnityEngine;
using System.Collections.Generic;
using System;

/// <summary>
/// 管理Resource相關，創造怪物，音效位置查詢等
/// </summary>
public class ResourcesManager
{
    static Dictionary<string , object> objList = new Dictionary<string, object>();

    #region Public

    /// <summary>
    /// 取得想指定的類型的Resources中的object，注意轉型錯誤。
    /// </summary>
    /// <typeparam name="T">想轉型的類別</typeparam>
    /// <param name="pathOrName">路徑或者 直接的名字(Resource下)</param>
    /// <returns></returns>
    public static object LoadAndCacheReource ( string pathOrName ) 
    {
        object obj = null;
        objList.TryGetValue( pathOrName , out obj );
        if ( obj == null )
        {
            obj = Resources.Load( pathOrName );
            if ( obj != null )
                objList.Add( pathOrName , obj );
        }

        return obj;
    }
    #endregion

    #region Private
    private static GameObject GetPrefab ( string Path )
    {
        return Resources.Load<GameObject>( Path );
    }
    #endregion
}