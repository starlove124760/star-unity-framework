﻿
using System.Collections.Generic;
using UnityEngine;

public class GameObjectManager
{
    protected List <GameObject> goList = new List<GameObject>();

    /// <summary>
    /// 可直接覆寫，供子類使用
    /// </summary>
    public virtual void StartSpawn ( ) { }

    /// <summary>
    /// 可直接覆寫，供子類使用
    /// </summary>
    public virtual void DeSpawn ( ) { }

    public virtual void RegGameObject ( GameObject _go , bool ShowDebug = false )
    {
        if ( goList.Contains( _go ) == false )
        {
            goList.Add( _go );
            if ( ShowDebug )
            {
                Debug.Log( string.Format( "註冊 : {0}" , _go ) );
                Debug.Log( string.Format( "{0} 目前已註冊總共 {1} 物件" ,
                    this.GetType().ToString() , goList.Count ) );
            }
        }
    }

    public virtual void RegGameObject ( List<GameObject> _goList , bool ShowDebug = false )
    {
        goList = _goList;
        if ( ShowDebug )
        {
            Debug.Log( string.Format( "[註冊],{0} 目前已註冊總共 {1} 物件" ,
                    this.GetType().ToString() , goList.Count ) );
        }
    }

    public virtual void UnRegGameObject ( GameObject _go , bool ShowDebug = false )
    {
        if ( goList.Contains( _go ) )
        {
            goList.Remove( _go );
            if ( ShowDebug )
                Debug.Log( string.Format( "[反註冊],{0} 目前已註冊總共 {1} 物件" ,
                       this.GetType().ToString() , goList.Count ) );
        }
    }

    public virtual void ClearAll ( )
    {
        if ( goList.Count > 0 )
        {
            goList.Clear();
        }
    }

    public int GetCount ( )
    {
        return goList.Count;
    }

    public List<GameObject> GetList ( )
    {
        return goList;
    }
}
