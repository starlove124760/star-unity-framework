﻿using System;
using UnityEngine;

public abstract class AbstractRoleControl : MonoBehaviour, iEventSender
{
    #region 需指定的物件
    protected AbstractAIContext _AbstractAIContext;
    protected Type _AIEnumType;
    #endregion

    protected Animator animator;
    protected AbstractSpawnControl _AbstractSpawnControl;

    public delegate void RoleControlEvent ( GameObject target );
    public event RoleControlEvent OnTargetChange;
    public event EventHandler<srGameEventArgs> EventUpdated;
    private srGameEventArgs m_srGameEventArgs = new srGameEventArgs();
    public readonly string Event_TargetChanged = "TargetChanged";
    public string CurAIState;

    public GameObject attackTarget;

    public GameObject AttackTarget
    {
        get {
            return attackTarget;
        }

        set {
            ChangeTarget( value );
            attackTarget = value;
        }
    }

    /// <summary>
    /// 取得Animator，得比狀態機還早取得否則會出錯。
    /// </summary>
    protected virtual void Start ( )
    {
        animator = GetComponent<Animator>();
        _AbstractAIContext = GetAbstractAIContext();
    }

    // Update is called once per frame
    protected virtual void Update ( )
    {
        //print( "Abstract Update" );
        if ( _AbstractAIContext != null )
            _AbstractAIContext.Update();
    }

    public void InjectManager ( AbstractSpawnControl tAbstractSpawnControl )
    {
        _AbstractSpawnControl = tAbstractSpawnControl;
    }

    public void StateChangeCallback ( string stateName )
    {
        CurAIState = stateName;
    }

    void AnimationEvent ( string msg )
    {
        //print( "Msg : " + msg );
        //if ( PhotonNetwork.isMasterClient )
        if ( _AbstractAIContext != null )
            _AbstractAIContext.AnimationEvent( msg );
    }

    protected abstract Type GetAIEnum ( );
    protected abstract AbstractAIContext GetAbstractAIContext ( );
    protected void ChangeTarget ( GameObject _target )
    {
        m_srGameEventArgs.evtName = Event_TargetChanged;
        m_srGameEventArgs.obj = _target;
        if ( EventUpdated != null )
            EventUpdated( this , m_srGameEventArgs );
    }

    /// <summary>
    /// 將在每個狀態呼叫base.Enter()時執行。
    /// </summary>
    /// <param name="tStateID">state id</param>
    /// <param name="t">Enum 的 Type</param>
    /// <returns></returns>
    public virtual bool PlayAction ( int tStateID )
    {
        if ( _AIEnumType == null )
        {
            _AIEnumType = GetAIEnum();
            if ( _AIEnumType == null )
                return false;
        }

        //Check Enum Value
        bool EnumIndication = Enum.IsDefined( _AIEnumType , tStateID );
        if ( EnumIndication == false )
            return false;
        if ( animator == null )
            return false;

        return true;
    }

    /// <summary>
    /// 將在每個狀態呼叫base.Exit()時執行。
    /// </summary>
    /// <param name="tStateID">state id</param>
    /// <param name="t">Enum 的 Type</param>
    /// <returns></returns>
    public virtual bool ExitAction ( int tStateID )
    {
        if ( _AIEnumType == null )
        {
            _AIEnumType = GetAIEnum();
            if ( _AIEnumType == null )
                return false;
        }

        //Check Enum Value
        bool EnumIndication = Enum.IsDefined( _AIEnumType , tStateID );
        if ( EnumIndication == false )
            return false;
        if ( animator == null )
            return false;

        return true;
    }

    public virtual void Die ( )
    {
        if ( _AbstractSpawnControl != null )
            _AbstractSpawnControl.RoleDie( gameObject );
        if ( _AbstractAIContext != null )
            _AbstractAIContext.Die();
    }
}
