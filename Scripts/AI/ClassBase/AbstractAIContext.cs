﻿
using Star_unity_framework;
using System;

public abstract class AbstractAIContext
{
    protected srMachineManager _AIManager;
    protected Action<string> StationChangedAction;

    public AbstractAIContext ( AbstractRoleControl tAbstractRoleControl )
    {
        _AIManager = new srMachineManager( new AI_Default() , tAbstractRoleControl );
        _AIManager.RegStateChangeCallback( tAbstractRoleControl.StateChangeCallback );
    }

    public virtual void AnimationEvent ( string msg )
    {
        AI_Base curAI_Base = _AIManager.GetCurMachineState() as AI_Base;
        if ( curAI_Base != null )
        {
            curAI_Base.AnimationEvent( msg );
        }
    }

    public virtual void Update ( )
    {
        _AIManager.Update();
    }

    public virtual void Die ( )
    {

    }

    /// <summary>
    /// 切換狀態
    /// </summary>
    /// <param name="tsrMachineStateBase">狀態的實例化</param>
    /// <param name="Obj"></param>
    public void ChangeState ( srMachineStateBase tsrMachineStateBase , object Obj = null )
    {
        _AIManager.ChangeState( tsrMachineStateBase , Obj );
    }

    /// <summary>
    /// 切換狀態
    /// </summary>
    /// <param name="StateId">狀態的int數值</param>
    /// <param name="Obj"></param>
    public void ChangeState ( int StateId , object Obj = null )
    {
        _AIManager.ChangeState( StateId , Obj );
    }

    protected void RegState ( AI_Base tAI_Base )
    {
        _AIManager.RegState( tAI_Base );
    }

    /// <summary>
    /// 切換預設的狀態
    /// </summary>
    /// <param name="tAI_Base"></param>
    public void ChangeDefaultState ( AI_Base tAI_Base )
    {
        _AIManager.ChangeDefaultState( tAI_Base );
    }

    public AI_Base GetCurAIState ( )
    {
        return _AIManager.GetCurMachineState() as AI_Base;
    }

    public void RegStateListener ( Action<string> _action )
    {
        StationChangedAction = _action;
    }
}
