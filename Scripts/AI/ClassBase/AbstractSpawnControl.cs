﻿
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 產生物件的基底類別
/// </summary>
public abstract class AbstractSpawnControl
{
    protected Dictionary<string , GameObject> _PrefabPool = new Dictionary<string, GameObject>();
    protected Dictionary<string , List<GameObject>> _InstanceGoPool = new Dictionary<string, List<GameObject>>();

    /// <summary>
    /// 角色死亡，從陣列移除
    /// </summary>
    /// <param name="_go"></param>
    public virtual void RoleDie ( GameObject _go )
    {
        List<GameObject> resultList = GetPoolByInstanceGO( _go );
        if(resultList!= null)
        {
            resultList.Remove( _go );
        }
    }

    /// <summary>
    /// 使用Resource.Load(PrefabName)，產生GameObject，並儲存起來
    /// </summary>
    /// <param name="prefabName"></param>
    /// <returns></returns>
    protected virtual GameObject SpawnGameObjectByName ( string prefabName )
    {
        List<GameObject> _goList;
        GameObject instanceGO = null;

        _PrefabPool.TryGetValue( prefabName , out instanceGO );
        _InstanceGoPool.TryGetValue( prefabName , out _goList );

        if ( instanceGO == null )
        {
            instanceGO = Resources.Load<GameObject>( prefabName );
            _PrefabPool.Add( prefabName , instanceGO );
        }

        GameObject go = Object.Instantiate( instanceGO , Vector3.zero , Quaternion.identity );
        if ( _goList == null )
        {
            _goList = new List<GameObject>();
            _goList.Add( go );
            _InstanceGoPool.Add( prefabName , _goList );
        }

        return go;
    }

    /// <summary>
    /// 取得實例化的陣列，透過Prefab名字
    /// </summary>
    /// <param name="prefabName"></param>
    /// <returns></returns>
    public List<GameObject> GetGameObjectListByName(string prefabName)
    {
        List<GameObject> goList = null;
        _InstanceGoPool.TryGetValue( prefabName ,out goList );
        return goList;
    }

    /// <summary>
    /// 取得實例化的陣列，透過實例化物件
    /// </summary>
    /// <param name="prefabName"></param>
    /// <returns></returns>
    protected List<GameObject> GetPoolByInstanceGO ( GameObject _go )
    {
        List<GameObject> resultList = null;
        
        foreach ( List<GameObject> _goList in _InstanceGoPool.Values )
        {
            if ( _goList.Contains( _go ) )
            {
                resultList = _goList;
                break;
            }
        }

        if ( resultList == null )
            Debug.LogError( "AbstractSpawnControl haven't Find : " + _go.name );
        return resultList;
    }
}
