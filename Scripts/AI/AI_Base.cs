﻿using Star_unity_framework;
using UnityEngine;

public class AI_Base : srMachineStateBase
{
    protected AbstractRoleControl _AbstractRoleControl;
    protected Transform transform;
    public AI_Base ( int iId ) : base( iId )
    {
    }

    public override void GetContext<T> ( T _Context ) 
    {
        base.GetContext( _Context );
        _AbstractRoleControl = _Context as AbstractRoleControl;
        if ( _AbstractRoleControl )
            transform = _AbstractRoleControl.transform;
        else
            Debug.LogError( "轉型失敗" );
    }

    public override void Enter ( object obj )
    {
        base.Enter( obj );
        _AbstractRoleControl.PlayAction( id );
    }

    public override void Exit ( )
    {
        base.Exit();
        _AbstractRoleControl.ExitAction( id );
    }

    public virtual void AnimationEvent ( string msg )
    {
    }
}

