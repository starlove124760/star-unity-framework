﻿using UnityEngine;

[System.Serializable]
public abstract class AbstractClass
{
        protected abstract bool Operation1 ();
        protected abstract void Operation2 ();
        protected abstract void Operation3 ();

        public void TemplateMethod ()
        {
                if ( Operation1() )
                {
                        Operation2();
                }
                else
                {
                        Operation3();
                }
        }
}

public class ConcreteClassA : AbstractClass
{
        protected override bool Operation1 ()
        {
                return true;
        }
        protected override void Operation2 ()
        {
                Debug.Log( "ConcreteClassA.Operation2" );
        }
        protected override void Operation3 ()
        {
                Debug.Log( "ConcreteClassA.Operation3" );
        }
}

public class ConcreteClassC : ConcreteClassA
{
        protected override bool Operation1 ()
        {
                return true;
        }
        protected override void Operation2 ()
        {
                Debug.Log( "ConcreteClassA.Operation2" );
        }
        protected override void Operation3 ()
        {
                Debug.Log( "ConcreteClassA.Operation3" );
        }
}

public class ConcreteClassB : AbstractClass
{
        protected override bool Operation1 ()
        {
                return false;
        }
        protected override void Operation2 ()
        {
                Debug.Log( "ConcreteClassB.Operation2" );
        }
        protected override void Operation3 ()
        {
                Debug.Log( "ConcreteClassB.Operation3" );
        }
}
