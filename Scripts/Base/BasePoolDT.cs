﻿using System.Collections.Generic;

namespace Star_unity_framework
{
    public class BasePoolDT<T>
    {
        protected T _id;

        public T id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public BasePoolDT<T> Clone ( )
        {
            return (BasePoolDT<T>)MemberwiseClone();
        }
    }
}
