﻿using Star_unity_framework;
using UnityEngine;

public class AIBase : srMachineStateBase
{
    public string name;
    public AIBase ( int iId ,string strName) : base( iId )
    {
        name = strName;
    }

    public override void Enter ( object obj )
    {
        base.Enter( obj );
        Debug.Log( "Enter : " +name );
    }

    public override void Excute ( )
    {   
        base.Excute();
        Debug.Log( "Excute : " + name );
    }

    public override void Exit ( )
    {
        base.Exit();
        Debug.Log( "Exit : " + name );
    }
}

