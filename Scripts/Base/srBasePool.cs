﻿using System.Collections.Generic;
using UnityEngine;

namespace Star_unity_framework
{
    public class srBasePool<T> where T : System.IComparable
    {
        private List<BasePoolDT<T>> listPoolDT = new List<BasePoolDT<T>>();
        private Dictionary<T , BasePoolDT<T>> dictionaryPoolDT;
        private string _RegDTName;
        private bool useDic;

        public string RegDTName
        {
            get
            {
                return _RegDTName;
            }
        }


        public srBasePool ( string tRegDTName , bool tUseDic = false )
        {
            _RegDTName = tRegDTName;
            useDic = tUseDic;
            if ( useDic == false )
                return;
            dictionaryPoolDT = new Dictionary<T , BasePoolDT<T>>();
        }

        public List<BasePoolDT<T>> GetList ( )
        {
            return listPoolDT;
        }

        public void Clear ( )
        {
            listPoolDT.Clear();
            if ( useDic )
            {
                dictionaryPoolDT.Clear();
            }
        }

        public List<BasePoolDT<T>> GetAllForId ( T data )
        {
            return listPoolDT.FindAll( x => x.Equals( data ) );
        }

        public BasePoolDT<T> GetForId ( T data )
        {
            BasePoolDT<T> basePoolDT = null;
            if ( useDic )
            {
                if ( dictionaryPoolDT.TryGetValue( data , out basePoolDT ) )
                {
                    if ( basePoolDT != null )
                        return basePoolDT;
                }
            }
            return listPoolDT.Find( x => x.id.Equals( data ) );
        }

        public bool CheckHaveData ( T data )
        {
            return GetForId( data ) != null;
        }

        public void Save ( BasePoolDT<T> tBastPoolDT  )
        {
            if ( GetForId( tBastPoolDT.id ) == null )
            {
                if ( useDic )
                    dictionaryPoolDT.Add( tBastPoolDT.id , tBastPoolDT );
                this.listPoolDT.Add( tBastPoolDT );
            }
            else
            {
                Debug.LogErrorFormat( "已註冊過此 ID : {0} , 類別名稱為 : {1}" , tBastPoolDT.id ,tBastPoolDT);
            }
        }

        public bool Delete ( T data )
        {
            BasePoolDT<T> basePoolDT;
            if ( useDic )
            {
                if ( dictionaryPoolDT.TryGetValue( data , out basePoolDT ) )
                    return false;
                dictionaryPoolDT.Remove( data );
            }
            BasePoolDT<T> forData = GetForId( data );
            if ( forData == null )
                return false;
            listPoolDT.Remove( forData );
            return true;
        }

        public bool SortById ( )
        {
            listPoolDT.Sort();
            return true;
        }
        
    }
}
