﻿using UnityEngine;
public class BaseLogClass
{
    protected void Log ( object message )
    {
        Debug.Log( message );
    }
    protected void Log ( object message , Object context )
    {
        Debug.Log( message , context );
    }
}
