﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomMono : MonoBehaviour
{
    List<MonoInterface> monoList = new List<MonoInterface>();
    // Use this for initialization
    void Start()
    {
        this.Init();
        for (int i = 0; i < monoList.Count; i++)
        {
            monoList[i].Init();
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < monoList.Count; i++)
        {
            monoList[i].f_Update();
        }
    }

    public virtual void Init()
    {

    }

    public void AddFakerMono(MonoInterface iMono)
    {
        if (monoList.Contains(iMono) == false)
        {
            monoList.Add(iMono);
        }
    }
}
