﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class FlowSeclector : MonoBehaviour {
    [ClassDropdownList(typeof(Base))]
    public string className;
	// Use this for initialization
	void Start () {
        Base baseClass = Assembly.GetExecutingAssembly().CreateInstance( className ) as Base;
    }
}
public class Base
{
}

public class A : Base
{
}
public class B : Base
{
}
public class C : Base
{
}