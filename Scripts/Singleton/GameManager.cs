﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public srMessage m_GameMessage = new srMessage();
    
    BaseMonoBehaviour[ ] BaseMonoBehaviourArray;
    List<MonoBehaviour> monoList = new List<MonoBehaviour>();

    //public KeyBoardInput m_KeyBoardInput = new KeyBoardInput();
    protected GameManager ( )
    {
    } // guarantee this will be always a singleton only - can't use the constructor!

    void Start ( )
    {
        monoList = GetComponents<MonoBehaviour>().ToList();
        BaseMonoBehaviourArray = GetComponents<BaseMonoBehaviour>();
        foreach ( var baseMonoBehaviour in BaseMonoBehaviourArray )
        {
            baseMonoBehaviour.OnStart();
        }
    }

    void Update ( )
    {
        foreach ( var baseMonoBehaviour in BaseMonoBehaviourArray )
        {
            baseMonoBehaviour.OnUpdate();
        }
    }

    public T GetMono<T>()
    {
        MonoBehaviour mono = null;
        Type type = typeof(T);
        for (int i = 0; i < monoList.Count; i++)
        {
            if (monoList[i].GetType().Equals(type))
            {
                mono = monoList[i];
                break;
            }
        }
        print(typeof(T));
        if (mono)
        {
            return (T)Convert.ChangeType(mono, typeof(T));
        }
        else
        {
            return default(T);
        }
    }
}