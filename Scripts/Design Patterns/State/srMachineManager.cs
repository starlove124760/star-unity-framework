﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star_unity_framework
{
    public class srMachineManager : srBasePool<int>
    {
        private srMachineStateBase currentStateBase;
        private srMachineStateBase srMachineStateBase_Default;
        private srMachineStateBase srMachineStateBase_Temp;

        private Action<string> StateChangedAction;
        private int id;
        private object obj;
        /// <summary>
        /// 紀錄持有Manager的Context
        /// </summary>
        private object obj_Context;

        /// <summary>
        /// 預設的初始狀態不執行Enter等行為。
        /// </summary>
        /// <param name="tBaseLoopState"></param>   
        public srMachineManager ( srMachineStateBase tBaseLoopState , object context )
            : base( "srMachineStateBase" , true )
        {
            if ( tBaseLoopState == null )
                Debug.LogError( "Loop狀態不能為空" );
            obj_Context = context;
            currentStateBase = null;
            srMachineStateBase_Default = tBaseLoopState;
            RegState( tBaseLoopState );
        }

        public void ChangeDefaultState ( srMachineStateBase tBaseLoopState )
        {
            srMachineStateBase_Default = tBaseLoopState;
            RegState( tBaseLoopState );
        }

        public void RegStateChangeCallback ( Action<string> _action )
        {
            StateChangedAction = _action;
        }

        public void RegState ( srMachineStateBase tsrMachineStateBase )
        {
            tsrMachineStateBase.InjectContext( obj_Context );
            tsrMachineStateBase.InjectionPool( this );
            Save( tsrMachineStateBase );
        }

        public srMachineStateBase GetStaticBase ( int iId )
        {
            if ( iId < 0 )
                return null;
            return (srMachineStateBase)GetForId( iId ) ?? null;
        }

        public void UnRegState ( srMachineStateBase tsrMachineStateBase )
        {
            Delete( tsrMachineStateBase.id );
        }

        public void UnRegState ( int iMachineStateId )
        {
            Delete( iMachineStateId );
        }

        public void ChangeState ( srMachineStateBase tsrMachineStateBase , object Obj = null )
        {
            if ( tsrMachineStateBase == null )
                Debug.LogError( "狀態數據非法" );
            if ( currentStateBase != null )
                currentStateBase.Exit();
            currentStateBase = tsrMachineStateBase;
            currentStateBase.InjectContext( obj_Context );
            //Debug.LogFormat( "Enter : {0}" , tsrMachineStateBase );
            currentStateBase.Enter( Obj );
            StateChangedAction( tsrMachineStateBase.ToString() );
        }

        public void ChangeState ( int StateId , object Obj = null )
        {
            srMachineStateBase_Temp = GetStaticBase( StateId );
            if ( srMachineStateBase_Temp == null )
            {
                Debug.LogErrorFormat( "Statid : {0} is No Exist" , StateId );
                ChangeState( srMachineStateBase_Default , null );
            }
            else
            {
                srMachineStateBase _srMachineStateBase = (srMachineStateBase)GetForId( StateId );
                ChangeState( _srMachineStateBase , Obj );
            }
        }

        public srMachineStateBase GetCurMachineState ( )
        {
            return currentStateBase;
        }

        public void Update ( )
        {
            if ( currentStateBase == null )
                return;
            if ( currentStateBase.IsComplete( ref id , ref obj ) )
            {
                srMachineStateBase_Temp = GetStaticBase( id );
                // - 目前狀態完成，去下一個指定的狀態
                if ( srMachineStateBase_Temp != null )
                    ChangeState( srMachineStateBase_Temp , obj );
                // - 如果抓不到下一個狀態，則回到初始狀態
                else
                    ChangeState( srMachineStateBase_Default , null );
            }
            else
                currentStateBase.Excute();
        }

        public void StopMachine ( )
        {
            Clear();
        }
    }
}
