﻿using UnityEngine;
using System.Collections;
using Star_unity_framework;
using UnityEngine.SceneManagement;

public enum StateSampleEnum
{
    DoNothing,
    flow_1,
    flow_2,
}
public class State_Unit : MonoBehaviour
{
    srMachineManager machineManager;

    // Use this for initialization
    void Start ( )
    {
        DontDestroyOnLoad( this );
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;

        machineManager = new srMachineManager( new DoNothing( StateSampleEnum.DoNothing ) , this);
        Flow_1 flow_1 = new Flow_1( StateSampleEnum.flow_1 );
        machineManager.RegState( flow_1 );
        machineManager.RegState( new Flow_2( StateSampleEnum.flow_2 ) );
        machineManager.ChangeState( machineManager.GetStaticBase( (int)StateSampleEnum.DoNothing ) );
    }

    private void SceneManager_activeSceneChanged ( Scene arg0 , Scene arg1 )
    {
        print( "Active SceneChanged To : " + arg1.name );
        if ( arg1.name == StateSampleEnum.flow_1.ToString() )
        {
            machineManager.ChangeState( machineManager.GetStaticBase( (int)StateSampleEnum.flow_1 ) );
        }
        if ( arg1.name == StateSampleEnum.flow_2.ToString() )
        {
            machineManager.ChangeState( machineManager.GetStaticBase( (int)StateSampleEnum.flow_2 ) );
        }
    }

    // Update is called once per frame
    void Update ( )
    {
        if ( Input.GetKeyDown( KeyCode.Escape ) )
        {
            print( "Enter" );
            machineManager.ChangeState( -99 );
        }
        else if ( Input.GetKeyDown( KeyCode.Backspace ) )
        {
            if ( machineManager != null )
            {
                machineManager.StopMachine();
                machineManager = null;
            }
        }
        if ( Input.GetKeyDown( KeyCode.Keypad1 ) )
        {

            SceneManager.LoadScene( StateSampleEnum.flow_1.ToString() );
        }
        else if ( Input.GetKeyDown( KeyCode.Keypad2 ) )
        {
            SceneManager.LoadScene( StateSampleEnum.flow_2.ToString() );
        }

        if ( machineManager != null )
            machineManager.Update();
    }


}
