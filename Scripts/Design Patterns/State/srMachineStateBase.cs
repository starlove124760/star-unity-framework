﻿using System.Collections.Generic;

namespace Star_unity_framework
{
    public class srMachineStateBase : BasePoolDT<int>
    {
        private int id_Next;
        private bool complete;
        private bool start;
        private object obj2;

        private srMachineManager machineManager;
        private object obj_Context;

        public srMachineStateBase ( int iId )
        {
            this.id = iId;
        }

        
        
        /// <summary>
        /// 這個狀態完成後呼叫，並指定下一個狀態以及傳遞的參數
        /// </summary>
        /// <param name="iId"></param>
        /// <param name="Obj"></param>
        public void SetCompleteToId ( int iId = -99 , object Obj = null )
        {
            complete = true;
            id_Next = iId;
            obj2 = Obj;
        }

        /// <summary>
        /// 如果完成，則將下一個要轉換的參數傳出去，並讓參數回到初始化。
        /// </summary>
        /// <param name="iId"></param>
        /// <param name="Obj"></param>
        /// <returns></returns>
        internal bool IsComplete ( ref int iId , ref object Obj )
        {
            if ( complete == false )
                return false;
            iId = id_Next;
            Obj = obj2;
            id_Next = -99;
            obj2 = null;
            return true;
        }

        internal void InjectionPool ( srMachineManager manager )
        {
            machineManager = manager;
        }

        internal void InjectContext ( object _Context )
        {
            obj_Context = _Context;
            GetContext( _Context );
        }

        public virtual void GetContext<T> ( T _Context )
        {

        }

        public virtual void Enter ( object obj )
        {
            complete = false;
            start = true;
        }

        public virtual void Excute ( )
        {
        }

        public virtual void Exit ( )
        {
            start = false;
        }

    }
}
