﻿using UnityEngine;

class Flow_1 : AIBase
{
    public Flow_1 ( StateSampleEnum e ) : base( (int)e , "Flow_1" )
    {
        
    }
    public override void Excute ( )
    {
        base.Excute();
        if ( Input.GetKeyDown(KeyCode.Space) )
        {
            Debug.Log( "Flow_1 Key" );
            SetCompleteToId(3);
        }
    }
}

