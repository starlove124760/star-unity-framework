﻿

using UnityEngine;

class DoNothing : AIBase
{
    public DoNothing ( StateSampleEnum e ) : base( (int)e , "DoNothing" )
    {

    }
    public override void Excute ( )
    {
        base.Excute();
        if ( Input.GetKeyDown( KeyCode.Space ) )
        {
            SetCompleteToId( 2 );
        }
    }
}
