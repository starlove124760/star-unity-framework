﻿using UnityEngine;

class Flow_2 : AIBase
{
    public Flow_2 ( StateSampleEnum e ) : base( (int)e , "Flow_2" )
    {

    }

    public override void Excute ( )
    {
        base.Excute();
        if ( Input.GetKeyDown( KeyCode.Space ) )
        {
            SetCompleteToId( 1 );
        }
    }
}

