﻿using UnityEngine;

/// <summary>
/// 榴彈碰撞到的物體的相關功能
/// </summary>
namespace Star_unity_framework.Design_Patterns.Strategy
{
    public interface ColliderStrategy
    {
        void TriggerEnter ( );
    }

    
    public class TriggeredRaptor : ColliderStrategy
    {
        public void TriggerEnter ( )
        {
            Debug.Log("TriggeredRaptor");
        }
    }

    public class TriggeredOilDrums : ColliderStrategy
    {
        public void TriggerEnter ( )
        {
            Debug.Log( "TriggeredOilDrums" );
        }
    }

    public class TriggeredTrex : ColliderStrategy
    {
        public void TriggerEnter ( )
        {
            Debug.Log("TriggeredTrex");
        }
    }
}
