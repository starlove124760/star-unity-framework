﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
/// <summary>
/// Dependency Injection 筆記 (3) http://www.huanlintalk.com/2011/10/dependency-injection-3.html
/// </summary>
namespace Star_unity_framework.Scripts.Design_Patterns.Decorator
{
    public interface ILogger
    {
        void Log ( string msg );
    }

    public class NullLogger : ILogger
    {
        public void Log ( string msg )
        {
            //Do Nothing
        }
    }

    public class ConsoleLogger : ILogger
    {
        public void Log ( string msg )
        {
            Debug.Log( msg );
        }
    }

    public class DecoratedLogger : ILogger
    {
        private ILogger logger;
        public DecoratedLogger ( ILogger _ILogger)
        {
            logger = _ILogger;
        }
        public void Log ( string msg )
        {
            logger.Log( string.Format( "{0} - {1}" , DateTime.Now.ToString() , msg ) );
        }
    }
}
