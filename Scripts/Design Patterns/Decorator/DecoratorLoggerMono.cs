﻿using UnityEngine;

namespace Star_unity_framework.Scripts.Design_Patterns.Decorator
{
    public class DecoratorLoggerMono : MonoBehaviour
    {

        // Use this for initialization
        void Start ( )
        {
            ShowLog( new ConsoleLogger() );
            ShowLog( new NullLogger() );
            ShowLog( new DecoratedLogger(new ConsoleLogger()) );
        }

        void ShowLog ( ILogger logger )
        {
            logger.Log( "Show Log" );
        }
    }
}
