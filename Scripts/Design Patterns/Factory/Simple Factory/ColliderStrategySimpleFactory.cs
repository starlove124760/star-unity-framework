﻿using System.Collections.Generic;
using Star_unity_framework.Design_Patterns.Strategy;
using Star_unity_framework;
public enum TriggerTarget
{
    [ClassInstance(typeof( TriggeredTrex))]
    Trex,
    [ClassInstance( typeof( TriggeredRaptor ) )]
    Raptor,
    [ClassInstance( typeof( TriggeredOilDrums) )]
    OilDrums,
}

namespace Star_unity_framework.Design_Patterns.Factory.Simple_Factory
{
    /// <summary>
    /// ColliderStrategy工廠
    /// </summary>
    public class ColliderStrategySimpleFactory
    {
        Dictionary <TriggerTarget , ColliderStrategy> dic = new Dictionary<TriggerTarget, ColliderStrategy>();

        private static ColliderStrategySimpleFactory instnce;
        public static ColliderStrategySimpleFactory Instance
        {
            get
            {
                if ( instnce == null )
                {
                    instnce = new ColliderStrategySimpleFactory();
                }
                return instnce;
            }
        }

        public static ColliderStrategySimpleFactory GetInstance ( )
        {
            return Instance;
        }

        public ColliderStrategySimpleFactory ( )
        {
            // OLD
            //dic.Add( TriggerTarget.Trex , new TriggeredTrex() );
            //dic.Add( TriggerTarget.Raptor , new TriggeredRaptor() );
            //dic.Add( TriggerTarget.OilDrums , new TriggeredOilDrums() );
        }

        public void RegStrategy ( TriggerTarget e , ColliderStrategy strategy )
        {
            if ( dic.ContainsKey( e ) == false )
            {
                dic.Add( e , strategy );
            }
        }

        public ColliderStrategy GetStrategy ( TriggerTarget type )
        {
            ColliderStrategy colliderStrategy;
            if ( dic.TryGetValue( type , out colliderStrategy ) )
            {
                return colliderStrategy;
            }
            return null;
        }
    }
}
