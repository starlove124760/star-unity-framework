﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Star_unity_framework.Design_Patterns.Factory.Simple_Factory;
using Star_unity_framework.Design_Patterns.Strategy;

/// <summary>
/// 測試策略+工廠模式
/// 不同物件被TriggerEnter事件
/// </summary>
public class StrategyMono : MonoBehaviour
{
    Dictionary < string , TriggerTarget > targets = new Dictionary<string , TriggerTarget>();
    // Use this for initialization
    void Start ( )
    {
        ColliderStrategySimpleFactory.Instance.RegStrategy( TriggerTarget.Trex , new TriggeredTrex() );
        ColliderStrategySimpleFactory.Instance.RegStrategy( TriggerTarget.Raptor , new TriggeredRaptor() );
        ColliderStrategySimpleFactory.Instance.RegStrategy( TriggerTarget.OilDrums , new TriggeredOilDrums() );


        TriggerTarget[] triggerTargets = EnumUtil.GetValues<TriggerTarget>();
        for ( int i = 0 ; i < triggerTargets.Length ; i++ )
        {
            ColliderStrategy strategy = (ColliderStrategy)System.Activator.CreateInstance(
           triggerTargets[ i ].GetTypeOfThisEnum() );
            strategy.TriggerEnter();
        }

        targets.Add( "Trex1" , TriggerTarget.Trex );
        targets.Add( "Raptor1" , TriggerTarget.Raptor );
        targets.Add( "Raptor2" , TriggerTarget.Raptor );
        targets.Add( "OilDrums1" , TriggerTarget.OilDrums );
    }

    // Update is called once per frame
    void Update ( )
    {

    }

    private void OnTriggerEnter ( Collider other )
    {
        string triggerName = other.name;
        TriggerTarget value;
        if ( targets.TryGetValue( triggerName , out value ) )
        {
            ColliderStrategy colliderStrategy = ColliderStrategySimpleFactory.Instance.GetStrategy( value );
            colliderStrategy.TriggerEnter();
        }
    }
}
