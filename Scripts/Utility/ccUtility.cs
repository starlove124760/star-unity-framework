﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

/// <summary>
/// 阿星's Utility Class
/// </summary>
public class srUtility
{
    public static void SetLookAtRotationNoY ( GameObject Self , GameObject Target )
    {
        Vector3 SelfPos = Self.transform.position;
        Vector3 TargetPos = Target.transform.position;

        SelfPos.y = 0;
        TargetPos.y = 0;
        SetLookAtRotation( Self , SelfPos , TargetPos );
    }
    public static void SetLookAtRotation ( GameObject Self , Vector3 SelfPos , Vector3 TargetPos )
    {
        Vector3 Sub = TargetPos - SelfPos;
        Quaternion q = Quaternion.LookRotation( Sub );
        Self.transform.rotation = q;
    }

    /// <summary>
    /// 回傳使用者所傳的陣列之中的一個
    /// </summary>
    public static T GetRandomValue<T>( params T[ ] array )
    {
        int RandomIndex = UnityEngine.Random.Range( 0 , array.Length );
        return array[ RandomIndex ];
    }

    /// <summary>
    /// 回傳0-最大值 之中的一個 不包含最大值,EX:100  => 0~99
    /// </summary>
    /// <param name="Count">max value</param>
    /// <returns></returns>
    public static int GetRandomValue ( int Count )
    {
        int RandomIndex = UnityEngine.Random.Range( 0 , Count );
        return RandomIndex;
    }

    /// <summary>
    /// 回傳使用者所傳的陣列之中的一個
    /// </summary>
    public static T GetRandomValue<T>( List<T> array )
    {
        int RandomIndex = UnityEngine.Random.Range( 0 , array.Count );
        return array[ RandomIndex ];
    }
    public static List<T> GetRandomValueFromList<T> ( int retunCount , List<T> _list )
    {
        var result = Enumerable.Range( 0 , _list.Count ).OrderBy(
     n => n * n * ( new System.Random() ).Next() ).Take( retunCount );
        List<int> RandList = result.ToList();
        List<T> returnList = new List<T>();
        for ( int i = 0 ; i < RandList.Count ; i++ )
        {
            int index = RandList[ i ];
            returnList.Add( _list[ index ] );
        }
        //returnList.ForEach( i => Debug.Log( i ) );
        return returnList;
    }

    /// <summary>
    /// 合併所有路徑為一個String
    /// </summary>
    /// <param name="paths"></param>
    /// <returns></returns>
    public static string PathCombine ( params string[ ] paths )
    {
        string CombinePath = paths[ 0 ];
        //Avoid Length Only 1
        if ( paths.Length > 1 )
        {
            for ( int i = 1 ; i < paths.Length ; i++ )
            {
                CombinePath = Path.Combine( CombinePath , paths[ i ] );
            }
        }
        return CombinePath;
    }

    /// <summary>
    /// yyyy/MM/dd
    /// </summary>
    /// <returns></returns>
    public static string GetTodayInfo ( )
    {
        return System.DateTime.Today.ToString( "yyyy/MM/dd" );
    }

    /// <summary>
    /// 檢查圓內LayerMask包含的遊戲物件，並執行ExplosionEnter中的Hit
    /// </summary>
    /// <param name="exploPos">爆炸的地點，提供計算用</param>
    /// <param name="radius">爆炸半徑</param>
    /// <param name="layerMask">爆炸會影響到的 Layer</param>
    /// <param name="exploTrans">爆炸物的Transform</param>
    public static void ExploOverlapSphere ( Vector3 exploPos , float radius ,
        LayerMask layerMask , Transform exploTransform )
    {
        Collider[ ] ExploColliderArray = Physics.OverlapSphere( exploPos , radius , layerMask.value );
        foreach ( var collider in ExploColliderArray )
        {
            ExplosionEnterBase explosionEnter = collider.GetComponentInParent<ExplosionEnterBase>();
            if ( explosionEnter )
            {
                explosionEnter.Hit( exploTransform );
            }
        }
    }

}
