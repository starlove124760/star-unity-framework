﻿using UnityEngine;

public class TestUtility : MonoBehaviour
{
        public enum abc
        {
                a,
                b,
                c
        }
        // Use this for initialization
        void Start ()
        {
                print( EnumUtil.GetValues<abc>() );
                print( EnumUtil.GetValues<abc>().Length );
                foreach ( var item in EnumUtil.GetValues<abc>() )
                {
                        print( item );
                }
        }

        // Update is called once per frame
        void Update ()
        {

        }
}
