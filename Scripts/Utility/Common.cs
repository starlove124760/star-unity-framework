﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class Common
{
    public static string ReadTextFileConfig ( string fileName )
    {
        return ReadTextFileFolder( "Config" , fileName );
    }

    public static string ReadTextFileFolder ( string folderName , string fileName )
    {
        string floderPath = Application.streamingAssetsPath + "/" + folderName;
        string filePath = Application.streamingAssetsPath + "/" + folderName +
            "/" + fileName + ".txt";
        //Debug.Log( Directory.Exists( floderPath ) );
        if ( Directory.Exists( floderPath ) == false )
        {
            DirectoryInfo di = Directory.CreateDirectory( floderPath );
            FileStream fr = File.Open( filePath , FileMode.OpenOrCreate );
            fr.Close();
            return string.Empty;
        }
        //Debug.Log( File.Exists( filePath ) );

        if ( File.Exists( filePath ) == false )
        {
            FileStream fr = File.Open( filePath , FileMode.OpenOrCreate );
            fr.Close();
            return string.Empty;
        }

        StreamReader sr = File.OpenText( filePath );

        string input = sr.ReadToEnd();
        sr.Close();
        return input;
    }

    public static string WriteTextFileConfig ( string fileName , string str )
    {
        return WriteTextFileFolder( "Config" , fileName , str );
    }

    public static string WriteTextFileFolder ( string folderName , string fileName , string str )
    {
        string path = Application.streamingAssetsPath + "/" + folderName +
            "/" + fileName + ".txt";

        if ( !File.Exists( path ) )
            return string.Empty;

        using ( StreamWriter outputFile = new StreamWriter( path , false ) )
        {
            outputFile.Write( str );
            outputFile.Close();
        }
        return str;
    }

    public static string ReadPhotonServerIPAddress ( )
    {
        return ReadTextFileConfig( "PhotonServerIPAddress" );
    }

    public static int ReadPlayCountTxt ( bool showDebugLog = false )
    {
        int i = 0;
        string s = ReadTextFileFolder( "Log" , "PlayCount" );
        string[] strs = s.Split( new string[] { Environment.NewLine } ,
            StringSplitOptions.RemoveEmptyEntries );
        string todayInfo = srUtility.GetTodayInfo();
        bool IsTodayDataExist = false;
        bool ContainToday = s.Contains( todayInfo );
        //Debug Info
        if ( showDebugLog )
        {
            Debug.Log( "ContainToday : " + ContainToday );
        }
        if ( ContainToday )
        {
            //Debug.Log( "Get Data" );
            foreach ( var str in strs )
            {
                if ( str.Contains( todayInfo ) )
                {
                    string[] correctLine = str.Split( ',' );
                    if ( correctLine.Length <= 1 )
                        i = 0;
                    else
                        IsTodayDataExist = int.TryParse( correctLine[ 1 ] , out i );
                    break;
                }
            }
        }
        if ( showDebugLog )
        {
            Debug.Log( "是否有今天的遊戲次數資料： " + IsTodayDataExist );
            Debug.Log( "次數: " + i );
        }

        return i;
    }

    public static void WritePlayCountTxt ( int PlayNum )
    {
        int PlayerCount = ReadPlayCountTxt() + PlayNum;
        string originStr = ReadTextFileFolder( "Log" , "PlayCount" );
        string todayInfo = srUtility.GetTodayInfo();
        string[] splitNewLintStrs = originStr.Split( new string[] { Environment.NewLine } ,
            StringSplitOptions.RemoveEmptyEntries );
        //Debug.Log( todayInfo );

        //New Data
        if ( originStr.Contains( todayInfo ) == false )
        {
            if ( originStr.Length > 0 )
            {
                bool haveSomthing = splitNewLintStrs.Length > 0;
                bool lastNotNewLine = originStr.Substring( originStr.Length - 1 ).Equals( "\n" ) == false;
                //Debug.Log( lastNotNewLine );

                if ( haveSomthing && lastNotNewLine )
                {
                    originStr += Environment.NewLine;
                }
            }
            originStr += srUtility.GetTodayInfo() + "," + PlayerCount;
        }
        //Override
        else
        {
            string[] splitTodayStrs = originStr.Split( new string[] { todayInfo } ,
            StringSplitOptions.RemoveEmptyEntries );
            string lastStr = string.Empty;

            originStr = splitTodayStrs[ 0 ] + todayInfo + "," + PlayerCount;
        }


        WriteTextFileFolder( "Log" , "PlayCount" , originStr );
    }

    public static void WritePhotonServerIPAddress ( string ipAddress )
    {
        WriteTextFileConfig( "PhotonServerIPAddress" , ipAddress.ToString() );
    }
}
