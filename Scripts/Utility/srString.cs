﻿
public class srString
{
    /// <summary>
    /// 取得彩色文字
    /// </summary>
    /// <param name="colorString">文字顏色色碼 9F0050</param>
    /// <returns></returns>
    public static string GetColorString ( string colorString , string original )
    {

        return "<color=#" + colorString + "ff>" + original + "</color>";
    }

    /// <summary>
    /// 取得粗體文字
    /// </summary>
    /// <param name="original"></param>
    /// <returns></returns>
    public static string GetBoldString(string original )
    {
        return "<b>" + original + "</b>";
    }

    /// <summary>
    /// 取得彩色又粗體的文字
    /// </summary>
    /// <param name="colorString">文字顏色色碼 9F0050</param>
    /// <param name="original"></param>
    /// <returns></returns>
    public static string GetBoldColorString ( string colorString , string original )
    {
        return "<b><color=#" + colorString + "ff>" + original + "</color></b>";
    }
}
