﻿using Star_unity_framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

/// <summary>
/// 處理Enum相關功能的Utility
/// </summary>
public static class EnumUtil
{
    /// <summary>
    /// 取得Enum中所有元素
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T[] GetValues<T>( )
    {
        return Enum.GetValues( typeof( T ) ).OfType<T>().ToArray();
    }

    /// <summary>
    /// 亂數取得指定Enum中隨便一個
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T GetEnumRandomValue<T>( )
    {
        return srUtility.GetRandomValue( GetValues<T>() );
    }

    /// <summary>
    /// 取得Enum 的 Description，搭配加上DescriptionAttribute Enum使用
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string GetEnumDescription ( this Enum source )
    {
        FieldInfo fi = source.GetType().GetField( source.ToString() );

        DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
            typeof( DescriptionAttribute ) , false );

        if ( attributes != null && attributes.Length > 0 )
            return attributes[ 0 ].Description;
        else
            return source.ToString();
    }
    /// <summary>
    /// 搭配 ClassInstance Attribute使用，取得Attribute註冊的Type，供動態實例化使用。
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Type GetTypeOfThisEnum ( this Enum value )
    {
        var enumType = value.GetType();
        var name = Enum.GetName( enumType , value );
        FieldInfo fi = value.GetType().GetField( value.ToString() );

        ClassInstanceAttribute[] attributes = (ClassInstanceAttribute[])fi.GetCustomAttributes(
            typeof( ClassInstanceAttribute ) , false );

        if ( attributes != null && attributes.Length > 0 )
            return attributes[ 0 ].type;
        return null;
    }

    public static int ToInt ( this Enum value )
    {
        return Convert.ToInt32( value );
    }

}

