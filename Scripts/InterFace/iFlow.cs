﻿
/// <summary>
/// 流程用的介面，包含事件監聽、開始流程方法
/// </summary>
public interface iFlow : iEventSender
{
    void StartFlow ( );
}
