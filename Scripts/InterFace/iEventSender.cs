﻿
using System;

//public delegate void GameEvent ( string evtName , object obj );

public class srGameEventArgs : EventArgs
{
    public string evtName
    {
        get; set;
    }
    public object obj
    {
        get; set;
    }

    public srGameEventArgs ( )
    {

    }

    public srGameEventArgs (string _evtName , object _obj = null )
    {
        evtName = _evtName;
        obj = _obj;
    }
}
/// <summary>
/// 發出訊息，給其他人監聽；利用EvtName去分類訊息，obj轉型成正確的資料
/// 同時也給Photon使用
/// </summary>
public interface iEventSender
{
    event EventHandler<srGameEventArgs> EventUpdated;
}
