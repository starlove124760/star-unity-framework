﻿using UnityEngine;
using System.Collections;

namespace ccCore
{
    public class UpdateInfo
    {
        public System.Action<UpdateInfo> m_Action;
        ccUpdateCaller m_ccUpdateManager;

        public UpdateInfo ( System.Action<UpdateInfo> action , ccUpdateCaller _ccUpdateManager )
        {
            m_Action = action;
            m_ccUpdateManager = _ccUpdateManager;
        }

        public void UnRegThisAction ( )
        {
            ccUpdateCaller.UnRegUpdate( this );
        }

        public void CallAction ( )
        {
            m_Action( this );
        }
    }
}