﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ccCore
{
    public class ccUpdateCaller : Singleton<ccUpdateCaller>
    {
        static List<UpdateInfo> m_UpdateInfo = new List<UpdateInfo>();

        // Update is called once per frame
        void Update ( )
        {
            if ( m_UpdateInfo.Count > 0 )
            {
                for ( int i = 0 ; i < m_UpdateInfo.Count ; i++ )
                {
                    m_UpdateInfo[ i ].CallAction();
                }
            }
        }

        public static void RegUpdate ( System.Action<UpdateInfo> action )
        {
            if ( m_UpdateInfo.Find( a => a.m_Action == action ) == null )
            {
                UpdateInfo updateInfo = new UpdateInfo( action , Instance );
                m_UpdateInfo.Add( updateInfo );
            }
        }

        public static void UnRegUpdate ( System.Action<UpdateInfo> action )
        {
            UpdateInfo updateInfo = m_UpdateInfo.Find( a => a.m_Action == action );
            if ( updateInfo != null )
            {
                m_UpdateInfo.Remove( updateInfo );
            }
        }

        public static void UnRegUpdate ( UpdateInfo updateInfo )
        {
            if ( updateInfo != null )
            {
                m_UpdateInfo.Remove( updateInfo );
            }
        }

        public static void UnRegAll ( )
        {
            m_UpdateInfo.Clear();
        }
    }
}