﻿using ccEngine;
using UnityEngine;

public class ccTaskCaller : MonoBehaviour
{
	ccCallback Pull_1;
	ccCallback Pull_2;
	public MeshRenderer cubeMeshrenderer_1;
	public MeshRenderer cubeMeshrenderer_2;
	Material cubeMeterial_1;
	Material cubeMeterial_2;

	// Use this for initialization
	void Start ()
	{
		cubeMeterial_1 = cubeMeshrenderer_1.material;
		cubeMeterial_2 = cubeMeshrenderer_2.material;
		//Pull_1 = ccTaskHandler.instance.f_RegOneChoiceTask( 
		//	function1 ,
		//	function2,
		//	function3,
		//	function4
		//	).finshedCallBack;

		//Pull_2 = ccTaskHandler.instance.f_RegOneChoiceTaskLoop( 
		//	function5 ,
		//	function6 ,
		//	function7 ,
		//	function8 ).finshedCallBack;

		//1,2,1,3,4
		Pull_1 = ccTaskHandler.instance.f_RegMultipleChoiceTask(
			new TaskData(0 , function1 , function4) ,
			new TaskData( 1 , function2 , function3 ),
			new TaskData( 2 , function1 )
			).finshedCallBack;
	}
	void function1 ()
	{
		Debug.Log( "Function1" );
		cubeMeterial_1.color = Color.red;
	}
	void function2 ()
	{
		Debug.Log( "Function2" );
		cubeMeterial_1.color = Color.blue;

	}
	void function3 ()
	{
		Debug.Log( "Function3" );
		cubeMeterial_1.color = Color.yellow;
	}
	void function4 ()
	{
		Debug.Log( "Function4" );
		cubeMeterial_1.color = Color.black;
	}

	void function5 ()
	{
		Debug.Log( "Function5" );
		cubeMeterial_2.color = Color.red;
	}
	void function6 ()
	{
		Debug.Log( "Function6" );
		cubeMeterial_2.color = Color.blue;
	}
	void function7 ()
	{
		Debug.Log( "function7" );
		cubeMeterial_2.color = Color.yellow;
	}
	void function8 ()
	{
		Debug.Log( "function8" );
		cubeMeterial_2.color = Color.black;
	}
	// Update is called once per frame
	void Update ()
	{
		if ( Input.GetKeyDown( KeyCode.Space ) )
			ccTaskHandler.instance.ShowTaskDictionary();
	}

	public void CallPull_1 ()
	{
		if ( Pull_1 != null )
		Pull_1();
	}
	public void CallPull_2 ()
	{
		if ( Pull_2 != null )
			Pull_2();
	}
}
