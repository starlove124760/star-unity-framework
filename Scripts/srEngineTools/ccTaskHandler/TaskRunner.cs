﻿using System.Collections;
using System.Collections.Generic;
using ccEngine;

public class TaskRunner
{
	List <ccCallback> callBackList = new List<ccCallback>();
	ccCallback callback;
	TaskRunner taskRunner;
	TaskInfo taskInfo;
	ccCallbackVT2<ccCallback , bool>startCoroutine;
	ccCallbackVT<ccCallback >   removeFromDirectionary;
	Dictionary<int , List<ccCallback>> multiplyTaskDic = new Dictionary<int , List<ccCallback>>();

	bool CanCallOnce,init;
	bool loop;
	bool manyWayTask;

	public TaskRunner ( bool IsLoop )
	{
		loop = IsLoop;
	}

	public TaskInfo Reg ( List<ccCallback> _callBackList ,
		ccCallbackVT2<ccCallback , bool> _startCoroutine , ccCallbackVT<ccCallback> _removeFromDirectionary )
	{
		callBackList = _callBackList;
		CanCallOnce = true;
		startCoroutine = _startCoroutine;
		removeFromDirectionary = _removeFromDirectionary;
		taskInfo = new TaskInfo( finshedCallBack );
		return taskInfo;
	}
	public TaskInfo Reg ( Dictionary<int , List<ccCallback>> _multiplyTaskDic ,
		ccCallbackVT2<ccCallback , bool> _startCoroutine , ccCallbackVT<ccCallback> _removeFromDirectionary )
	{
		manyWayTask = true;
		multiplyTaskDic = _multiplyTaskDic;
		CanCallOnce = true;
		startCoroutine = _startCoroutine;
		removeFromDirectionary = _removeFromDirectionary;
		taskInfo = new TaskInfo( finshedCallBack );
		return taskInfo;
	}

	void finshedCallBack ()
	{
		//System.Diagnostics.StackFrame frame = new System.Diagnostics.StackFrame( 1 );
		//var method = frame.GetMethod();
		//var type = method.DeclaringType;
		//var name = method.Name;
		//UnityEngine.Debug.Log( method );
		//UnityEngine.Debug.Log( type );
		//UnityEngine.Debug.Log( name );
		//UnityEngine.Debug.Log ( "StackTrace: " +  System.Environment.StackTrace );
		CanCallOnce = true;
		if ( init == false )
		{
			startCoroutine( finshedCallBack , manyWayTask );
			init = true;
		}
	}

	public void Run ()
	{
		CanCallOnce = true;
	}

	public IEnumerator aaa ()
	{
		int callBackCount = callBackList.Count;
		bool bRun = true;
		while ( bRun )
		{
			yield return null;
			if ( CanCallOnce )
			{
				int workFlowIndex = taskInfo.workFlowIndex;
				callBackList[ workFlowIndex ]();
				CanCallOnce = false;
				taskInfo.workFlowIndex++;
				if ( taskInfo.workFlowIndex >= callBackCount )
				{
					if ( loop )
						taskInfo.workFlowIndex = 0;
					else
						bRun = false;
				}
			}
		}
		UnityEngine.Debug.Log( "closed" );
		removeFromDirectionary( finshedCallBack );
	}

	public IEnumerator bbb ()
	{
		int callBackCount = multiplyTaskDic.Count;
		bool bRun = true;
		while ( bRun )
		{
			yield return null;
			if ( CanCallOnce )
			{
				int level = taskInfo.iLevel;
				int workFlowIndex = taskInfo.workFlowIndex;
				//Debug.Log( "level : " + level );
				//Debug.Log( "workFlowIndex : " + workFlowIndex );
				multiplyTaskDic[ level ][ workFlowIndex ]();
				CanCallOnce = false;
				taskInfo.workFlowIndex++;
				taskInfo.LevelDic[ level ] = workFlowIndex;

				//IF Have Next , Jump To Next && next store index != nextTaskCout
				if ( multiplyTaskDic.ContainsKey( level + 1 ) &&
					taskInfo.LevelDic[ level + 1] != multiplyTaskDic[ level + 1 ].Count )
				{
					taskInfo.iLevel++;
					taskInfo.workFlowIndex = 0;
				}

				//該流程index >= 儲存的流程數量
				if ( taskInfo.workFlowIndex >= multiplyTaskDic[ level ].Count )
				{
					if ( multiplyTaskDic.ContainsKey( level - 1 ) )
					{
						taskInfo.iLevel--;
						taskInfo.workFlowIndex = taskInfo.LevelDic[ taskInfo.iLevel ]+1;
						//set full index
						taskInfo.LevelDic[ level  ] = multiplyTaskDic[ level ].Count;
					}
					else
						bRun = false;
				}
				//+++ About Loop
				//if ( taskInfo.workFlowIndex >= callBackCount )
				//{
				//	if ( loop )
				//		taskInfo.workFlowIndex = 0;
				//	else
				//		bRun = false;
				//}
			}
		}
		UnityEngine.Debug.Log( "closed" );
		removeFromDirectionary( finshedCallBack );
	}
}
