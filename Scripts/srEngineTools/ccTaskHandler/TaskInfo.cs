﻿using System.Collections.Generic;
using System.Linq;
using ccEngine;
public class TaskInfo 
{
	public ccCallback finshedCallBack;
	public int workFlowIndex;
	public int iLevel;
	public Dictionary<int , int> LevelDic = new Dictionary<int, int>();

	public TaskInfo ( ccCallback _finshedCallBack )
	{
		finshedCallBack = _finshedCallBack;
	}
}

public struct TaskData
{
	public int iLevel;
	public List<ccCallback> callBackList;

	public TaskData (int Level , params ccCallback[] _callback)
	{
		iLevel = Level;
		callBackList = _callback.ToList();
	}

}
