﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class srTools
{
    static string str = "  ,  ";
    [Conditional( "RELEASE" )]
    public static void Show2IntDebug ( params int[] a  )
    {
        string s = string.Empty;
        for ( int i = 0 ; i < a.Length ; i++ )
        {
            int v3 = a[ i ];
            s += i == a.Length - 1 ? v3.ToString() : v3 + str;
        }
        UnityEngine.Debug.Log( s );
    }
    [Conditional( "RELEASE" )]
    public static void Show2Vector3Debug (params Vector3 [] a)
    {
        string s = string.Empty;
        for ( int i = 0 ; i < a.Length ; i++ )
        {
            Vector3 v3 = a[ i ];
            s += i == a.Length - 1 ? v3.ToString() : v3 + str;
        }
        UnityEngine.Debug.Log( s );
    }
    [Conditional( "RELEASE" )]
    public static void Show2floatDebug ( params float[] a )
    {
        string s = string.Empty;
        for ( int i = 0 ; i < a.Length ; i++ )
        {
            float f = a[ i ];
            s += i == a.Length - 1 ? f.ToString() : f + str;
        }
        UnityEngine.Debug.Log( s );
    }
}
