﻿using UnityEngine;

public class GetAnimationEnd : MonoBehaviour
{
    public Animator anim;
    public string stateName;
    public int CallBackCount = 3;
    public bool IsLoop;
    public bool IsInfinite;
    bool bRun;
    public void OnEnable ( )
    {

        if ( IsLoop )
            ccEngine.ccAnimationEvent.Instance.f_RegEventForMultipleTimes( anim , stateName , end , CallBackCount );
        else if ( IsInfinite )
            ccEngine.ccAnimationEvent.Instance.f_RegEventForLoop( anim , stateName , end );
        else
            ccEngine.ccAnimationEvent.Instance.f_RegEventForOnce( anim , stateName , end );
    }

    // Use this for initialization
    void Start ( )
    {

    }

    private void end ( int excuTimes , Animator _Anim )
    {
        print( _Anim.name + " : complete Times : " + excuTimes );
        //如果上面註冊時沒有填寫，則這時候可以手動解除事件，傳入該Animator
        //if ( excuTimes >= 10 )
        //    ccEngine.ccAnimationEvent.Instance.f_UnRegEvent( _Anim );
    }
    void Update ( )
    {
        if (   Input.GetKeyDown(KeyCode.Space))
        {
            ccEngine.ccAnimationEvent.Instance.f_UnRegEvent( anim );
        }
    }
}