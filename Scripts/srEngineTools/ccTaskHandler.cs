﻿using System.Collections.Generic;
using System.Linq;
using ccEngine;
using UnityEngine;

public class ccTaskHandler : MonoBehaviour
{
	public static ccTaskHandler instance;
	List<TaskRunner> taskRunnerList = new List<TaskRunner>();
	Dictionary<ccCallback , TaskRunner> TaskDictionary= new Dictionary<ccCallback, TaskRunner>();
	ccCallbackVT2<ccCallback , bool> startCoroutine;
	ccCallbackVT<ccCallback> removeFormDictionary;


	void Awake ()
	{
		instance = this;
		startCoroutine = callCoroutine;
		removeFormDictionary = removeAt;
	}

	public TaskInfo f_RegOneChoiceTask ( params ccCallback[] _callBack )
	{
		return RegOneWayTask( false , _callBack );
	}

	public TaskInfo f_RegOneChoiceTaskLoop ( params ccCallback[] _callBack )
	{
		return RegOneWayTask( true , _callBack );
	}

	public TaskInfo f_RegMultipleChoiceTask ( params TaskData[] _taskData )
	{
		return RegManyWayTask( false , _taskData );
	}

	TaskInfo RegOneWayTask ( bool loop , ccCallback[] _callBack )
	{
		TaskRunner tr = new TaskRunner( loop );
		taskRunnerList.Add( tr );
		TaskInfo taskInfo = tr.Reg( _callBack.ToList() , startCoroutine , removeFormDictionary );

		ccCallback FinshedFunction = taskInfo.finshedCallBack;
		TaskDictionary.Add( FinshedFunction , tr );
		return taskInfo;
	}

	TaskInfo RegManyWayTask ( bool loop , TaskData[] _taskDataArray )
	{
		Dictionary<int , List<ccCallback>> MultiplyTaskDic = new Dictionary<int , List<ccCallback>>();
		foreach ( var taskData in _taskDataArray )
		{
			int level = taskData.iLevel;
			List<ccCallback> callBackList = taskData.callBackList;
			if ( MultiplyTaskDic.ContainsKey( level ) == false )
			{
				MultiplyTaskDic.Add( level , callBackList );
			}
		}

		TaskRunner tr = new TaskRunner( loop );
		taskRunnerList.Add( tr );
		TaskInfo taskInfo = tr.Reg( MultiplyTaskDic , startCoroutine , removeFormDictionary );
		//init taskInfo.LevelDic
		for ( int i = 0 ; i < MultiplyTaskDic.Count ; i++ )
		{
			taskInfo.LevelDic.Add( i , 0 );
		}

		ccCallback FinshedFunction = taskInfo.finshedCallBack;
		TaskDictionary.Add( FinshedFunction , tr );
		return taskInfo;
	}

	void callCoroutine ( ccCallback _callBack , bool manyWayTask )
	{
		TaskRunner tr = TaskDictionary[ _callBack ];
		if ( manyWayTask )
			StartCoroutine( tr.bbb() );
		else
			StartCoroutine( tr.aaa() );
	}

	void removeAt ( ccCallback _callback )
	{
		TaskDictionary.Remove( _callback );
	}

	public void ShowTaskDictionary ()
	{
		print( "TaskDictionary Count : " + TaskDictionary.Count );
	}

	public ccCallback f_RegTask ( params TaskParms[] args )
	{
		TaskRunner_Mutiple trm = new TaskRunner_Mutiple();
		ccCallback FinshedFunction = trm.Reg( args );
		StartCoroutine( trm.aaa() );
		return FinshedFunction;
	}
}

public class TaskParms
{
	public int iLevel;
	public ccCallback callBack;

	public TaskParms ( int _iLevel , ccCallback _callBack )
	{
		iLevel = _iLevel;
		callBack = _callBack;
	}
}