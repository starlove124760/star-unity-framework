﻿/********************************************************************
	created:	2017/06/14
	created:	14:6:2017   11:28
	filename: 	star-unity-framework\Scripts\Extensions\GameObjectExtensions.cs
	file path:	star-unity-framework\Scripts\Extensions
	file base:	GameObjectExtensions
	file ext:	cs
	author:		Star
	
	purpose:	UnityObject的Extensions
*********************************************************************/

namespace star.framework.Extensions
{
    using System.Collections.Generic;
    using UnityEngine;
    public static class UnityObjectExtensions
    {
        public static GameObject Find ( this GameObject go , string nameToFind , bool bSearchInChildren )
        {
            if ( bSearchInChildren )
            {
                var transform = go.transform;
                var childCount = transform.childCount;
                //Debug.LogError( go.name + " ChildCount: " + childCount);
                for ( int i = 0 ; i < childCount ; ++i )
                {
                    var child = transform.GetChild( i );
                    if ( child.gameObject.name == nameToFind )
                        return child.gameObject;
                    GameObject result = child.gameObject.Find( nameToFind , bSearchInChildren );
                    if ( result != null )
                        return result;
                }
                return null;
            }
            else
            {
                return GameObject.Find( nameToFind );
            }
        }

        /// <summary>Unity-version-independent replacement for active GO property.</summary>
        /// <returns>Unity 3.5: active. Any newer Unity: activeInHierarchy.</returns>
        public static bool GetActive ( this GameObject target )
        {
            return target.activeInHierarchy;
        }

        /// <summary>Set postion to target</summary>
        public static void SetPostion ( this Transform self , GameObject target )
        {
            self.position = target.transform.position;
        }

        /// <summary>Set postion to target</summary>
        public static void SetPostion ( this GameObject self , GameObject target )
        {
            self.transform.position = target.transform.position;
        }

        /// <summary>Set rotation to target</summary>
        public static void SetRotation ( this Transform self , GameObject target )
        {
            self.rotation = target.transform.rotation;
        }

        /// <summary>Set rotation to target</summary>
        public static void SetRotation ( this GameObject self , GameObject target )
        {
            self.transform.rotation = target.transform.rotation;
        }

        /// <summary>Set postion and rotation to target</summary>
        public static void SetPostionAndRotation ( this GameObject self , GameObject target )
        {
            self.transform.position = target.transform.position;
            self.transform.rotation = target.transform.rotation;
        }

        /// <summary>Set postion and rotation to target</summary>
        public static void SetPostionAndRotation ( this Transform self , GameObject target )
        {
            self.position = target.transform.position;
            self.rotation = target.transform.rotation;
        }

        public static List<GameObject> GetAllChildGameObject ( this GameObject self )
        {
            int ChildCount = self.transform.childCount;
            List<GameObject> goList = new List<GameObject>();
            for ( int i = 0 ; i < ChildCount ; i++ )
            {
                goList.Add( self.transform.GetChild( i ).gameObject );
            }
            return goList;
        }

        public static List<Transform> GetAllChildTransform ( this GameObject self )
        {
            int ChildCount = self.transform.childCount;
            List<Transform> transList = new List<Transform>();
            for ( int i = 0 ; i < ChildCount ; i++ )
            {
                transList.Add( self.transform.GetChild( i ) );
            }
            return transList;
        }

        public static List<GameObject> GetAllChildGameObject ( this Transform self )
        {
            int ChildCount = self.childCount;
            List<GameObject> goList = new List<GameObject>();
            for ( int i = 0 ; i < ChildCount ; i++ )
            {
                goList.Add( self.GetChild( i ).gameObject );
            }
            return goList;
        }

        public static List<Transform> GetAllChildTransform ( this Transform self )
        {
            int ChildCount = self.childCount;
            List<Transform> transList = new List<Transform>();
            for ( int i = 0 ; i < ChildCount ; i++ )
            {
                transList.Add( self.GetChild( i ) );
            }
            return transList;
        }

        /// <summary>
        /// 取得包含containName的子物件集合
        /// </summary>
        /// <param name="self"></param>
        /// <param name="containName"></param>
        /// <returns></returns>
        public static List<Transform> GetAllChildTransformByName ( this Transform self , string containName )
        {
            int ChildCount = self.childCount;
            List<Transform> transList = new List<Transform>();
            for ( int i = 0 ; i < ChildCount ; i++ )
            {
                Transform trans = self.GetChild( i );
                if ( trans.name.Contains( containName ) )
                {
                    transList.Add( trans );
                }
            }
            return transList;
        }
    }
}